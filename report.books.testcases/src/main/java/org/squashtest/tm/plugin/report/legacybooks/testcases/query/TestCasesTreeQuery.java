/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.query;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.api.repository.SqlQueryRunner;
import org.squashtest.tm.api.utils.CurrentUserHelper;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.CufBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.Dataset;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.DatasetParamValue;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.LinkedRequirementsBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.NodeBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.Parameter;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.TestCaseBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.TestCaseStepsBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.foundation.CriteriaEntry;
import org.squashtest.tm.plugin.report.legacybooks.testcases.foundation.CufType;
import org.squashtest.tm.plugin.report.legacybooks.testcases.foundation.EntityType;
import org.squashtest.tm.plugin.report.legacybooks.testcases.foundation.ReportOptions;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class TestCasesTreeQuery implements ReportQuery, InitializingBean {

	private static final String MILESTONE_ID = "milestoneId";
	private static final String MILESTONE_LABEL = "milestoneLabel";
	private static final String TREE_PICKER = "TREE_PICKER";
	private static final String MILESTONE_PICKER = "MILESTONE_PICKER";
	private static final String TAG_PICKER = "TAG_PICKER";
	private static final String DATA = "data";
	private static final String SQL_FIND_MILESTONE_LABEL = "select label from MILESTONE where milestone_id = :milestoneId";

	private Collection<NodeBean> nodesBeans;

	private SqlQueryRunner runner;
	private TestCasesTreeQueryFinder queryFinder = new TestCasesTreeQueryFinder();
	private TestCasesTreeFormatter formatter = new TestCasesTreeFormatter();
	private CurrentUserHelper currentUserHelper;
	@Override
	public void afterPropertiesSet() throws Exception {
		queryFinder.setRunner(runner);
	}

	public void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	public void setCurrentUserHelper(CurrentUserHelper currentUserHelper) {
		this.currentUserHelper = currentUserHelper;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.squashtest.tm.api.report.query.ReportQuery#executeQuery(java.util .Map, java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	public void executeQuery(Map<String, Criteria> crit, Map<String, Object> res) {

		nodesBeans = Collections.emptyList();
		Map<String, Boolean> optionsMap = new HashMap<String, Boolean>();

		Criteria options = crit.get(CriteriaEntry.REPORT_OPTIONS.getValue());
		optionsMap.put(ReportOptions.PRINT_STEPS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_LINKED_REQUIREMENTS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_CALL_STEPS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_PARAMETERS.getValue(), false);
		optionsMap.put(ReportOptions.PRINT_STEP_CUFS.getValue(), false);

		Collection<String> selectedOptions = (Collection<String>) options.getValue();

		for (String option : selectedOptions) {
			optionsMap.put(option, true);
		}

		Collection<Object[]> rawNodesData = getNodeBean(crit);
		nodesBeans = formatter.toNodesBean(rawNodesData);
        Collection<Long> tcIds = getTcIds(crit);

		if (!tcIds.isEmpty()) {
			Collection<Object[]> rawTestCaseData = queryFinder.getTestCasesData(tcIds);
			boolean printSteps = optionsMap.get(ReportOptions.PRINT_STEPS.getValue());

			Collection<LinkedRequirementsBean> linkedRequirementsBeans = new ArrayList<>();
			if (optionsMap.get(ReportOptions.PRINT_LINKED_REQUIREMENTS.getValue())) {
				Collection<Object[]> rawLinkedReqData = queryFinder.getLinkedReqData(tcIds);
				linkedRequirementsBeans = formatter.toLinkedRequirementBean(rawLinkedReqData);
			}
			Collection<Parameter> parameters = new ArrayList<>();
			Collection<Dataset> datasets = new ArrayList<>();
			Collection<DatasetParamValue> datasetParamValuesData = new ArrayList<>();
			if (optionsMap.get(ReportOptions.PRINT_PARAMETERS.getValue())) {
				Collection<Object[]> rawParametersData = queryFinder.getParametersData(tcIds);
				parameters = formatter.toParameter(rawParametersData);
				Collection<Object[]> rawDataSetData = queryFinder.getDataSetData(tcIds);
				datasets = formatter.toDataSet(rawDataSetData);
				Collection<Object[]> rawDataSetParamValuesData = queryFinder.getDataSetParamValueData(tcIds);
				datasetParamValuesData = formatter.toDataSetParamValue(rawDataSetParamValuesData);
			}

			Collection<TestCaseStepsBean> testCaseStepsBeans = new ArrayList<>();
			Map<String, Collection<CufBean>> stepCufMap = new HashMap<>();
			if (optionsMap.get(ReportOptions.PRINT_STEPS.getValue())) {
				Collection<Object[]> rawTestCaseStepsData = queryFinder.getTestCaseStepsData(tcIds);
				testCaseStepsBeans = formatter.toTestCaseSteps(rawTestCaseStepsData);
				if (optionsMap.get(ReportOptions.PRINT_CALL_STEPS.getValue())) {
					Collection<TestCaseStepsBean> caseStepsBeans = new ArrayList<>();
					addCalledSteps(testCaseStepsBeans, caseStepsBeans);
					testCaseStepsBeans = caseStepsBeans;
				}

				List<Long> stepIds = new ArrayList<>();
				for (TestCaseStepsBean steps : testCaseStepsBeans) {
					stepIds.add(steps.getId());
				}
				Collection<CufBean> stepCuf = new ArrayList<>();
				Collection<CufBean> stepNumCuf = new ArrayList<>();
				Collection<CufBean> stepTagCuf = new ArrayList<>();
				Collection<CufBean> stepRtfCuf = new ArrayList<>();
				if (!stepIds.isEmpty() && optionsMap.get(ReportOptions.PRINT_STEP_CUFS.getValue())) {
					Collection<Object[]> rawStepCuf = queryFinder.getCufData(EntityType.TEST_STEP.name(), stepIds);
					Collection<Object[]> rawStepNumCuf = queryFinder.getNumCufData(EntityType.TEST_STEP.name(), stepIds);
					Collection<Object[]> rawStepTagCuf = queryFinder.getTagCufData(EntityType.TEST_STEP.name(), stepIds);
					Collection<Object[]> rawStepRtfCuf = queryFinder.getRtfCufData(EntityType.TEST_STEP.name(), stepIds);
					stepCuf = formatter.toCufBean(rawStepCuf);
					stepNumCuf = formatter.toCufBean(rawStepNumCuf);
					stepTagCuf = formatter.toTagCufBean(rawStepTagCuf);
					stepRtfCuf = formatter.toCufBean(rawStepRtfCuf);
				}
				stepCufMap.put(CufType.CUFS.getValue(), stepCuf);
				stepCufMap.put(CufType.NUM_CUFS.getValue(), stepNumCuf);
				stepCufMap.put(CufType.TAG_CUFS.getValue(), stepTagCuf);
				stepCufMap.put(CufType.RTF_CUFS.getValue(), stepRtfCuf);
			}
			Map<String, Collection<CufBean>> testCaseCufMap = new HashMap<>();
			Collection<Object[]> rawTestCaseCuf = queryFinder.getCufData(EntityType.TEST_CASE.name(), tcIds);
			Collection<CufBean> testCaseCuf = formatter.toCufBean(rawTestCaseCuf);
			testCaseCufMap.put(CufType.CUFS.getValue(), testCaseCuf);
			Collection<Object[]> rawTestCaseNumCuf = queryFinder.getNumCufData(EntityType.TEST_CASE.name(), tcIds);
			Collection<CufBean> testCaseNumCuf = formatter.toCufBean(rawTestCaseNumCuf);
			testCaseCufMap.put(CufType.NUM_CUFS.getValue(), testCaseNumCuf);
			Collection<Object[]> rawTestCaseTagCuf = queryFinder.getTagCufData(EntityType.TEST_CASE.name(), tcIds);
			Collection<CufBean> testCaseTagCuf = formatter.toTagCufBean(rawTestCaseTagCuf);
			testCaseCufMap.put(CufType.TAG_CUFS.getValue(), testCaseTagCuf);
			Collection<Object[]> rawTestCaseRtfCuf = queryFinder.getRtfCufData(EntityType.TEST_CASE.name(), tcIds);
			Collection<CufBean> testCaseRtfCuf = formatter.toCufBean(rawTestCaseRtfCuf);
			testCaseCufMap.put(CufType.RTF_CUFS.getValue(), testCaseRtfCuf);

			Collection<TestCaseBean> testCaseBeans = formatter.toTestCaseBean(rawTestCaseData, printSteps);
			formatter.bindAll(testCaseStepsBeans, testCaseCufMap, stepCufMap, linkedRequirementsBeans,
				testCaseBeans, parameters, datasets, datasetParamValuesData, nodesBeans);
		}
		res.put(DATA, nodesBeans);
		// special milestone mode
		if (isMilestonePicker(crit)) {

			List<Integer> milestoneIds = (List<Integer>) crit.get(CriteriaEntry.MILESTONES.getValue()).getValue();

			Map<String, Object> params = new HashMap<>();
			params.put(MILESTONE_ID, milestoneIds.get(0));

			String milestoneLabel = runner.executeUniqueSelect(SQL_FIND_MILESTONE_LABEL, params);

			res.put(MILESTONE_LABEL, milestoneLabel);
		}
	}

	// This method can be optimized if we process by layer but it's more difficult to link call step to the good test case
	private void addCalledSteps(Collection<TestCaseStepsBean> stepsBeans, Collection<TestCaseStepsBean> allSteps) {

		List<Long> tcAlreadyAdd = new ArrayList<>();
		for(TestCaseStepsBean testCaseStepsBean : stepsBeans) {
			if("C".equals(testCaseStepsBean.getType())) {
				Long id = testCaseStepsBean.getId();

				Collection<Object[]> objects = queryFinder.getTestCaseStepsData(id);
				Collection<TestCaseStepsBean> testCaseStepsBeans = formatter.toCallStepBean(objects,testCaseStepsBean.getTestCaseId());
				tcAlreadyAdd.add(testCaseStepsBean.getTestCaseId());
				addCalledSteps(testCaseStepsBeans, allSteps);

			} else {
				allSteps.add(testCaseStepsBean);
			}
		}
	}

    public void setIdsByProjectQuery(Resource idsByProjectQuery) {
        String query = loadQuery(idsByProjectQuery);
        queryFinder.setIdsByProjectQuery(query);
    }

    public void setIdsByTagQuery(Resource idsByTagQuery) {
        String query = loadQuery(idsByTagQuery);
        queryFinder.setIdsByTagQuery(query);
    }

    public void setIdsBySelectionQuery(Resource idsBySelectionQuery) {
        String query = loadQuery(idsBySelectionQuery);
        queryFinder.setIdsBySelectionQuery(query);
    }

    public void setIdsByMilestoneQuery(Resource idsByMilestoneQuery) {
        String query = loadQuery(idsByMilestoneQuery);
        queryFinder.setIdsByMilestoneQuery(query);
    }

	public void setTestCaseDataQuery(Resource testCaseDataQuery) {
		String query = loadQuery(testCaseDataQuery);
		queryFinder.setTestCasesQuery(query);
	}

	public void setLinkedReqDataQuery(Resource linkedReqDataQuery) {
		String query = loadQuery(linkedReqDataQuery);
		queryFinder.setLinkedReqQuery(query);
	}

	public void setTestCaseStepsDataQuery(Resource testCaseStepsDataQuery) {
		String query = loadQuery(testCaseStepsDataQuery);
		queryFinder.setTestCaseStepsQuery(query);
	}

	public void setCufsDataQuery(Resource cufsDataQuery) {
		String query = loadQuery(cufsDataQuery);
		queryFinder.setCufQuery(query);
	}
	public void setNumCufsDataQuery(Resource numCufsDataQuery) {
		String query = loadQuery(numCufsDataQuery);
		queryFinder.setNumCufQuery(query);
	}
	public void setTagCufsDataQuery(Resource tagCufsDataQuery) {
		String query = loadQuery(tagCufsDataQuery);
		queryFinder.setTagCufQuery(query);
	}
	public void setRtfCufsDataQuery(Resource rtfCufsDataQuery) {
		String query = loadQuery(rtfCufsDataQuery);
		queryFinder.setRtfCufQuery(query);
	}

	public void setNodesByTestCaseIdsDataQuery(Resource nodesByTestCaseIdsDataQuery) {
		String query = loadQuery(nodesByTestCaseIdsDataQuery);
		queryFinder.setNodesByTestCaseIdsQuery(query);
	}
	public void setNodesByMilestonesDataQuery(Resource nodesByMilestonesDataQuery) {
		String query = loadQuery(nodesByMilestonesDataQuery);
		queryFinder.setNodesByMilestonesQuery(query);
	}
	public void setNodesByTagsDataQuery(Resource nodesByTagsDataQuery) {
		String query = loadQuery(nodesByTagsDataQuery);
		queryFinder.setNodesByTagsQuery(query);
	}
	public void setNodesByProjectIdsDataQuery(Resource nodesByProjectIdsDataQuery) {
		String query = loadQuery(nodesByProjectIdsDataQuery);
		queryFinder.setNodesByProjectIdsQuery(query);
	}

	public void setStepsByCallStepDataQuery(Resource stepsByCallStepDataQuery) {
		String query = loadQuery(stepsByCallStepDataQuery);
		queryFinder.setTestStepsByCallSteps(query);
	}

	public void setParameterDataQuery(Resource parameterDataQuery) {
		String query = loadQuery(parameterDataQuery);
		queryFinder.setParametersQuery(query);
	}

	public void setDataSetDataQuery(Resource dataSetDataQuery) {
		String query = loadQuery(dataSetDataQuery);
		queryFinder.setDataSetQuery(query);
	}

	public void setDataSetParamValueDataQuery(Resource dataSetParamValueDataQuery) {
		String query = loadQuery(dataSetParamValueDataQuery);
		queryFinder.setDataSetParamValueQuery(query);
	}

    private Collection<Long> getTcIds(Map<String, Criteria> criteriaMap) {
        Criteria selectionMode = criteriaMap.get(CriteriaEntry.TEST_CASES_SELECTION_MODE.getValue());

        Collection<Long> testCaseIdList = Collections.emptyList();
        List<Long> readableProjectIds = currentUserHelper.findReadableProjectIds();
        if (TREE_PICKER.equals(selectionMode.getValue())) {
            Criteria idsCrit = criteriaMap.get(CriteriaEntry.TEST_CASE_IDS.getValue());
            Collection<String> testCaseIds = addNodesIds(idsCrit);
            testCaseIdList = queryFinder.findIdsBySelection(testCaseIds, readableProjectIds);
        } else if (MILESTONE_PICKER.equals(selectionMode.getValue())) {
            Collection<String> milestones = (Collection<String>) criteriaMap.get(CriteriaEntry.MILESTONES.getValue()).getValue();
            testCaseIdList = queryFinder.findIdsByMilestone(milestones, readableProjectIds);
        } else if (TAG_PICKER.equals(selectionMode.getValue())) {
            Collection<String> tags = (Collection<String>) criteriaMap.get(CriteriaEntry.TAGS.getValue()).getValue();
            testCaseIdList = queryFinder.findIdsByTags(tags, readableProjectIds);
        } else {
            Criteria idsCrit = criteriaMap.get(CriteriaEntry.PROJECT_IDS.getValue());
            if (!idsCrit.getValue().toString().trim().isEmpty()) {
                List<Long> projectIds = ((Collection<String>) idsCrit.getValue())
                    .stream()
                    .map(Long::valueOf)
                    .toList();
                readableProjectIds.retainAll(projectIds);
                testCaseIdList = queryFinder.findIdsByProject(readableProjectIds);
            }
        }

        return testCaseIdList;
    }

	private Collection<Object[]> getNodeBean(Map<String, Criteria> criteriaMap) {
		Criteria selectionMode = criteriaMap.get(CriteriaEntry.TEST_CASES_SELECTION_MODE.getValue());
		Collection<Object[]> result = new ArrayList<>();
        List<Long> readableProjectIds = currentUserHelper.findReadableProjectIds();
		if (TREE_PICKER.equals(selectionMode.getValue())) {
			Criteria idsCrit = criteriaMap.get(CriteriaEntry.TEST_CASE_IDS.getValue());
			Collection<String> testCaseIds = addNodesIds(idsCrit);
			result = queryFinder.getNodesByTestCasesIds(testCaseIds, readableProjectIds);
		} else if (MILESTONE_PICKER.equals(selectionMode.getValue())) {
			Collection<String> milestoneIds = (Collection<String>) criteriaMap.get(CriteriaEntry.MILESTONES.getValue()).getValue();
			result = queryFinder.getNodesByMilestones(milestoneIds, readableProjectIds);
		} else if (TAG_PICKER.equals(selectionMode.getValue())){
			Collection<String> tags = (Collection<String>) criteriaMap.get(CriteriaEntry.TAGS.getValue()).getValue();
			result = queryFinder.getNodesTags(tags, readableProjectIds);
		} else {
			Criteria idsCrit = criteriaMap.get(CriteriaEntry.PROJECT_IDS.getValue());
			if (!idsCrit.getValue().toString().trim().isEmpty()) {
				List<Long> projectIds = ((List<String>) idsCrit.getValue())
                    .stream()
                    .map(Long::valueOf)
                    .toList();
                readableProjectIds.retainAll(projectIds);
				result = queryFinder.getNodesByProjectIds(readableProjectIds);
			}
		}

		return result;

	}

	/**
	 * Should return a list of Node Ids (in this case : requirement Ids) selected from the form.
	 *
	 * @param idsCrit
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection addNodesIds(Criteria idsCrit) {
		if (idsCrit != null) {
			Collection<?> ids = ((Map<String, Collection<?>>) idsCrit.getValue()).values();
			if (!ids.isEmpty()) {
				Collection nodesIds = new HashSet<String>();
				nodesIds.addAll(ids);
				return nodesIds;
			}
			return Collections.emptyList();
		}
		return Collections.emptyList();
	}
	private boolean isMilestonePicker(Map<String, Criteria> criteriaMap) {
		Criteria selectionMode = criteriaMap.get(CriteriaEntry.TEST_CASES_SELECTION_MODE.getValue());

		return MILESTONE_PICKER.equals(selectionMode.getValue());
	}


	protected String loadQuery(Resource query) {
		InputStream is;
		try {
			is = query.getInputStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return new Scanner(is, "UTF-8").useDelimiter("\\A").next();
	}

}
