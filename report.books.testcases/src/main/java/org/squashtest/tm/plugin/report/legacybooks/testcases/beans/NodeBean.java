/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.beans;

import org.springframework.web.util.HtmlUtils;

import java.util.List;

public class NodeBean implements Comparable<NodeBean>{

	private Long itemId;

	private Long projectId;

	private String projectName;

	private List<TestCaseBean> testCasesBeans;

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public String getUnescapedProjectName() {
		return HtmlUtils.htmlUnescape(projectName);
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public void setTestCasesBeans(List<TestCaseBean> testCasesBeans) {
		this.testCasesBeans = testCasesBeans;
	}

	public List<TestCaseBean> getTestCasesBeans() {
		return testCasesBeans;
	}

	//should sort by project first, then by the sorting chain.
	//note : according to the result returned by the sql query the best ranked testcasebean for each node, is the
	//first one of the list
	public int compareTo(NodeBean bean) {

		int comparison;

		comparison = projectName.compareTo(bean.projectName);

		if (comparison == 0){
			TestCaseBean myHiLevelBean = testCasesBeans.get(0);
			TestCaseBean otherHiLevelBean = bean.getTestCasesBeans().get(0);
			comparison = myHiLevelBean.getSortingChain().compareTo(otherHiLevelBean.getSortingChain());
		}

		return comparison;

	}

}
