/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.squashtest.tm.api.repository.SqlQueryRunner;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TestCasesTreeQueryFinder {

	private static final String TAGS = "tags";
	private static final String MILESTONES = "milestones";
	private static final String ENTITY_TYPE = "entityType";
	private static final String ENTITY_IDS = "entityIds";
	private static final String PROJECT_IDS = "projectIds";
	private static final String NODE_IDS = "nodeIds";
	private static final String TEST_CASE_IDS = "testcaseIds";
	private static final String TEST_CASE_ID = "testcaseId";
    private String idsByProjectQuery;
    private String idsBySelectionQuery;
    private String idsByMilestoneQuery;
    private String idsByTagQuery;
    private String testCasesQuery;
	private String linkedReqQuery;
	private String testCaseStepsQuery;
	private String cufQuery;
	private String rtfCufQuery;
	private String tagCufQuery;
	private String numCufQuery;
	private String nodesByTestCaseIdsQuery;
	private String nodesByMilestonesQuery;
	private String nodesByTagsQuery;
	private String nodesByProjectIdsQuery;
	private String testStepsByCallSteps;
	private String parametersQuery;
	private String dataSetQuery;
	private String dataSetParamValueQuery;

	SqlQueryRunner runner;

    void setIdsByProjectQuery(String idsByProjectQuery) {
        this.idsByProjectQuery = idsByProjectQuery;
    }

    void setIdsBySelectionQuery(String idsBySelectionQuery) {
        this.idsBySelectionQuery = idsBySelectionQuery;
    }

    void setIdsByMilestoneQuery(String idsByMilestoneQuery) {
        this.idsByMilestoneQuery = idsByMilestoneQuery;
    }

    void setIdsByTagQuery(String idsByTagQuery) {
        this.idsByTagQuery = idsByTagQuery;
    }

	void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	void setTestCasesQuery(String testCasesQuery) {
		this.testCasesQuery = testCasesQuery;
	}

	void setTestCaseStepsQuery(String testCaseStepsQuery) {
		this.testCaseStepsQuery = testCaseStepsQuery;
	}

	void setLinkedReqQuery(String linkedReqQuery) {
		this.linkedReqQuery = linkedReqQuery;
	}

	void setCufQuery(String cufQuery) {
		this.cufQuery = cufQuery;
	}

	void setRtfCufQuery(String rtfCufQuery) {
		this.rtfCufQuery = rtfCufQuery;
	}

	void setTagCufQuery(String tagCufQuery) {
		this.tagCufQuery = tagCufQuery;
	}

	void setNumCufQuery(String numCufQuery) {
		this.numCufQuery = numCufQuery;
	}

	void setNodesByTestCaseIdsQuery(String nodesByTestCaseIdsQuery) {
		this.nodesByTestCaseIdsQuery = nodesByTestCaseIdsQuery;
	}

	void setNodesByMilestonesQuery(String nodesByMilestonesQuery) {
		this.nodesByMilestonesQuery = nodesByMilestonesQuery;
	}

	void setNodesByTagsQuery(String nodesByTagsQuery) {
		this.nodesByTagsQuery = nodesByTagsQuery;
	}

	void setNodesByProjectIdsQuery(String nodesByProjectIdsQuery) {
		this.nodesByProjectIdsQuery = nodesByProjectIdsQuery;
	}

	void setTestStepsByCallSteps(String testStepsByCallSteps) {
		this.testStepsByCallSteps = testStepsByCallSteps;
	}

	void setParametersQuery(String parametersQuery) {
		this.parametersQuery = parametersQuery;
	}

	void setDataSetQuery(String dataSetQuery) {
		this.dataSetQuery = dataSetQuery;
	}

	void setDataSetParamValueQuery(String dataSetParamValueQuery) {
		this.dataSetParamValueQuery = dataSetParamValueQuery;
	}

    Collection<Long> findIdsByProject(List<Long> projectIds) {
        if (CollectionUtils.isEmpty(projectIds)) {
            return Collections.emptyList();
        }

        Map<String, Collection<Long>> params = new HashMap<>(1);
        params.put(PROJECT_IDS, projectIds);

        List<BigInteger>foundStrIds = runner.executeSelect(idsByProjectQuery, params);

        return toIdList(foundStrIds);
    }

    Collection<Long> findIdsBySelection(Collection<String> ids, List<Long> projectIds) {

        List<Long> versionIds = new LinkedList<>();

        // add the requirements within the given folders
        if (ids != null && (!ids.isEmpty())) {

            Map<String, Collection<Long>> params = new HashMap<>(1);
            params.put(NODE_IDS, toIdList(ids));
            params.put(PROJECT_IDS, projectIds);
            List<BigInteger> foundStrIds = runner.executeSelect(idsBySelectionQuery, params);

            versionIds.addAll(toIdList(foundStrIds));
        } else {
            versionIds = new ArrayList<>();
        }

        return versionIds;

    }

    Collection<Long> findIdsByMilestone(Collection<String> milestoneIds, List<Long> projectIds) {

        if (!milestoneIds.isEmpty()) {
            Map<String, Collection<Long>> params = new HashMap<>();
            params.put(MILESTONES, toIdList(milestoneIds));
            params.put(PROJECT_IDS, projectIds);
            List<BigInteger> foundIds = runner.executeSelect(idsByMilestoneQuery, params);
            return toIdList(foundIds);
        } else {
            return Collections.emptyList();
        }

    }

    Collection<Long> findIdsByTags(Collection<String> tags, List<Long> projectIds) {

        if (!tags.isEmpty()) {
            Map<String, Object> params = new HashMap<>();
            params.put(TAGS, tags);
            params.put(PROJECT_IDS, projectIds);
            List<BigInteger> foundIds = runner.executeSelect(idsByTagQuery, params);
            return toIdList(foundIds);
        } else {
            return Collections.emptyList();
        }
    }

	Collection<Object[]> getTestCasesData(Collection<Long> testCaseIds) {
		Map<String, Object> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(testCasesQuery, params);
	}

	Collection<Object[]> getLinkedReqData(Collection<Long> testCaseIds) {
		Map<String, Object> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(linkedReqQuery, params);
	}

	Collection<Object[]> getTestCaseStepsData(Collection<Long> testCaseIds) {
		Map<String, Object> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(testCaseStepsQuery, params);
	}

	Collection<Object[]> getCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds, cufQuery);
	}

	Collection<Object[]> getNumCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds, numCufQuery);
	}

	Collection<Object[]> getRtfCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds, rtfCufQuery);
	}

	Collection<Object[]> getTagCufData(String entityType, Collection<Long> entityIds) {
		return executeCufQuery(entityType, entityIds, tagCufQuery);
	}

	Collection<Object[]> executeCufQuery(String entityType, Collection<Long> entityIds, String query) {
		Map<String, Object> params = new HashMap<>();
		params.put(ENTITY_TYPE, entityType);
		params.put(ENTITY_IDS, entityIds);
		return execute(query, params);
	}

	Collection<Object[]> getNodesByTestCasesIds(Collection<String> testCaseIds, List<Long> projectIds) {
		Map<String, Object> params = new HashMap<>(1);
		params.put(NODE_IDS, toIdList(testCaseIds));
        params.put(PROJECT_IDS, projectIds);
		return execute(nodesByTestCaseIdsQuery, params);
	}

	Collection<Object[]> getNodesByMilestones(Collection<String> milestones, List<Long> projectIds) {
		Map<String, Object> params = new HashMap<>();
		params.put(MILESTONES, toIdList(milestones));
		params.put(PROJECT_IDS, projectIds);
		return execute(nodesByMilestonesQuery, params);
	}

	Collection<Object[]> getNodesTags(Collection<String> tags, List<Long> projectIds) {
		Map<String, Object> params = new HashMap<>();
		params.put(TAGS, tags);
		params.put(PROJECT_IDS, projectIds);
		return execute(nodesByTagsQuery, params);
	}

	Collection<Object[]> getNodesByProjectIds(List<Long> projectIds) {
		Map<String, Object> params = new HashMap<>(1);
		params.put(PROJECT_IDS, projectIds);
		return execute(nodesByProjectIdsQuery, params);
	}

	Collection<Object[]> getTestCaseStepsData(Long testCaseId) {
		Map<String, Object> params = new HashMap<>();
		params.put(TEST_CASE_ID, testCaseId);
		return execute(testStepsByCallSteps, params);
	}

	Collection<Object[]> getParametersData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(parametersQuery, params);
	}

	Collection<Object[]> getDataSetData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(dataSetQuery, params);
	}

	Collection<Object[]> getDataSetParamValueData(Collection<Long> testCaseIds) {
		Map<String, Collection<Long>> params = new HashMap<>();
		params.put(TEST_CASE_IDS, testCaseIds);
		return execute(dataSetParamValueQuery, params);
	}


	private Collection<Object[]> execute(String query, Map<String, ?> params) {
		return runner.executeSelect(query, params);
	}

	@SuppressWarnings("unchecked")
	Collection<Long> toIdList(Collection<?> ids) {
		return CollectionUtils.collect(ids, new IdTransformer());
	}

	// dirty impl
	private static class IdTransformer implements Transformer {
		public Object transform(Object arg0) {
			Class<?> argClass = arg0.getClass();

			if (argClass.equals(String.class)) {
				return Long.valueOf((String) arg0);
			} else if (argClass.equals(BigInteger.class)) {
				return ((BigInteger) arg0).longValue();
			} else if (argClass.equals(Integer.class)) {
				return Long.valueOf((Integer) arg0);
			} else {
				throw new RuntimeException("bug : IdTransformer cannto convert items of class " + argClass.getName());
			}
		}
	}
}
