/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.beans;

public class Parameter {

	private String name;
	private String description;
	private Long testCaseId;
	private String testCaseSourceName;
	private String testCaseSourceRef;
	private String testCaseSourceProjectName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getTestCaseSourceName() {
		String testCaseName = "";
		if(!testCaseSourceName.isEmpty()) {
			testCaseName = testCaseSourceRef + "-"+ testCaseSourceName+ " (" + testCaseSourceProjectName+")";
		}
		return testCaseName;
	}

	public void setTestCaseSourceName(String testCaseSourceName) {
		this.testCaseSourceName = testCaseSourceName;
	}

	public String getTestCaseSourceRef() {
		return testCaseSourceRef;
	}

	public void setTestCaseSourceRef(String testCaseSourceRef) {
		this.testCaseSourceRef = testCaseSourceRef;
	}

	public String getTestCaseSourceProjectName() {
		return testCaseSourceProjectName;
	}

	public void setTestCaseSourceProjectName(String testCaseSourceProjectName) {
		this.testCaseSourceProjectName = testCaseSourceProjectName;
	}
}
