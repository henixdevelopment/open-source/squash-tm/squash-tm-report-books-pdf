/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.foundation;

import java.util.List;

public class Paragraph {

	private int previousLevel = -1;

	private int indexLevel0 = 0;

	private Long previousProjectId = 0L;

	private Long currentProjectId = 0L;

	public void calculateParagraphLevel(String level,
			List<Integer> paragraphLevel) {
		int currentLevel = Integer.parseInt(level);

		if (!currentProjectId.equals(previousProjectId)) {
			indexLevel0 = 0;
			previousProjectId = currentProjectId;
		}
		if (currentLevel == 0) {
			indexLevel0++;
			paragraphLevel.removeAll(paragraphLevel);
			paragraphLevel.add(currentLevel, indexLevel0);
		} else if (currentLevel > previousLevel) {
			try{
				paragraphLevel.add(currentLevel, 1);
			} catch(java.lang.IndexOutOfBoundsException exception){
				int padding = currentLevel - paragraphLevel.size();
				for(int i=0; i<padding; i++){
					paragraphLevel.add(1);
				}
				paragraphLevel.add(currentLevel, 1);
			}
		} else if (currentLevel == previousLevel) {
			paragraphLevel.set(currentLevel,
					paragraphLevel.get(currentLevel) + 1);
		} else if (currentLevel < previousLevel) {
			paragraphLevel.set(currentLevel,
					paragraphLevel.get(currentLevel) + 1);

			int paragraphLevelSize = paragraphLevel.size();

			for (int index = paragraphLevelSize - 1; index > currentLevel ; index--) {
				paragraphLevel.remove(index);
			}

		}
		previousLevel = currentLevel;
		previousProjectId = currentProjectId;
	}

	public void setCurrentProjectId(Long currentProjectId) {
		this.currentProjectId = currentProjectId;
	}




}
