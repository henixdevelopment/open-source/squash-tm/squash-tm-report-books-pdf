/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.query;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.CufBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.Dataset;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.DatasetParamValue;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.LinkedRequirementsBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.NodeBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.Parameter;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.TestCaseBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.beans.TestCaseStepsBean;
import org.squashtest.tm.plugin.report.legacybooks.testcases.foundation.CufType;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class TestCasesTreeFormatter {

	private static final String END_SEPARATOR_PLACEHOLDER = "=Sep=";
	private static final String SORTING_CHAIN_SEPARATOR = " ";
	private static final String CHAIN_SEPARATOR = " > ";
	private static final String SEPARATOR_PLACEHOLDER = END_SEPARATOR_PLACEHOLDER + ",";
	private static final String TAG_EMPTY = "";
	private static final String TAG_SEPARATOR = " | ";
	private static final Integer POSITION = 1;

	private static final String MILESTONE_SEPARATOR = "," + SORTING_CHAIN_SEPARATOR;

	private List<Integer> paragraphs;

	private List<Long> tcIdsAlreadyAdd;

	private Long projectId;

	public Collection<TestCaseStepsBean> toTestCaseSteps(Collection<Object[]> testCaseStepsData) {

		Collection<TestCaseStepsBean> result = new ArrayList<>();
		for (Object[] array: testCaseStepsData) {
			TestCaseStepsBean testCaseStepsBean = new TestCaseStepsBean();
			testCaseStepsBean.setId(evaluateExpressionToLong(array[0]));
			testCaseStepsBean.setAction(evaluateExpressionToString(array[1]));
			testCaseStepsBean.setExpectedResult(evaluateExpressionToString(array[2]));
			testCaseStepsBean.setType(evaluateExpressionToString(array[3]));
			testCaseStepsBean.setOrder(evaluateExpressionToLong(array[4]));
			testCaseStepsBean.setDataset(evaluateExpressionToString(array[5]));
			testCaseStepsBean.setTestCaseId(evaluateExpressionToLong(array[6]));
			testCaseStepsBean.setDelegateParameterValues(evaluateExpressionToBoolean(array[7]));
			result.add(testCaseStepsBean);
		}
		return result;
	}

	public Collection<CufBean> toCufBean(Collection<Object[]> cufsData) {

		Collection<CufBean> result = new ArrayList<>();
		for (Object[] array: cufsData) {
			CufBean cufBean = new CufBean();
			cufBean.setValue(evaluateExpressionToString(array[0]));
			cufBean.setLabel(evaluateExpressionToString(array[1]));
			cufBean.setType(evaluateExpressionToString(array[2]));
			cufBean.setEntityId(evaluateExpressionToLong(array[3]));
			result.add(cufBean);
		}
		return result;
	}

	Collection<CufBean> toTagCufBean(Collection<Object[]> tagCufData) {
		Collection<CufBean> cufs = new ArrayList<>();
		for (Object[] row : tagCufData) {
			CufBean cuf = new CufBean();
			if (row[0] != null) {
				String chain = evaluateExpressionToString(row[0]).replaceAll(SEPARATOR_PLACEHOLDER, TAG_SEPARATOR);
				cuf.setValue(evaluateExpressionToString(chain.substring(0, chain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			} else {
				cuf.setValue(TAG_EMPTY);
			}
			cuf.setLabel(evaluateExpressionToString(row[1]));
			cuf.setType(evaluateExpressionToString(row[2]));
			cuf.setEntityId(evaluateExpressionToLong(row[3]));
			cufs.add(cuf);
		}
		return cufs;
	}

	public Collection<LinkedRequirementsBean> toLinkedRequirementBean(Collection<Object[]> linkedRequirementData) {

		Collection<LinkedRequirementsBean> result = new ArrayList<>();
		for(Object[] array: linkedRequirementData) {
			LinkedRequirementsBean linkedRequirementsBean = new LinkedRequirementsBean();
			linkedRequirementsBean.setId(evaluateExpressionToLong(array[0]));
			linkedRequirementsBean.setName(evaluateExpressionToString(array[1]));
			linkedRequirementsBean.setProjectName(evaluateExpressionToString(array[2]));
			linkedRequirementsBean.setCriticality(evaluateExpressionToString(array[3]));
			linkedRequirementsBean.setVersion(evaluateExpressionToLong(array[4]));
			linkedRequirementsBean.setReference(evaluateExpressionToString(array[5]));
			linkedRequirementsBean.setTestCaseId(evaluateExpressionToLong(array[6]));
			result.add(linkedRequirementsBean);
		}
		return result;
	}

	public Collection<TestCaseBean> toTestCaseBean(Collection<Object[]> testCaseData, boolean printSteps) {

		Collection<TestCaseBean> result = new ArrayList<>();
		for(Object[] array: testCaseData) {
			TestCaseBean testcase = new TestCaseBean();
			String chain = evaluateExpressionToString(array[0]).replaceAll(SEPARATOR_PLACEHOLDER, CHAIN_SEPARATOR);
			testcase.setChain(HtmlUtils.htmlEscape(chain.substring(0, chain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			testcase.setId(evaluateExpressionToLong(array[1]));
			testcase.setFolder(evaluateExpressionToLong(array[2]));
			testcase.setName(HtmlUtils.htmlEscape(evaluateExpressionToString(array[3])));
			testcase.setLevel(evaluateExpressionToLong(array[4]));
			testcase.setImportance(evaluateExpressionToString(array[5]));
			testcase.setNature(evaluateExpressionToString(array[6]));
			testcase.setType(evaluateExpressionToString(array[7]));
			testcase.setNatureType(evaluateExpressionToString(array[8]));
			testcase.setTypeType(evaluateExpressionToString(array[9]));
			testcase.setStatus(evaluateExpressionToString(array[10]));
			testcase.setExecutionMode(evaluateExpressionToLong(array[11]));
			testcase.setPrerequisites(evaluateExpressionToString(array[12]));
			testcase.setReference(HtmlUtils.htmlEscape(evaluateExpressionToString(array[13])));
			testcase.setCreatedOn(evaluateExpressionToString(array[14]));
			testcase.setCreatedBy(evaluateExpressionToString(array[15]));
			testcase.setLastModifiedOn(evaluateExpressionToString(array[16]));
			testcase.setLastModifiedBy(evaluateExpressionToString(array[17]));
			testcase.setDescription(evaluateExpressionToString(array[18]));
			testcase.setAttachments(evaluateExpressionToLong(array[19]));
			String sortingChain = evaluateExpressionToString(array[20]).replaceAll(SEPARATOR_PLACEHOLDER,
				SORTING_CHAIN_SEPARATOR);
			testcase.setSortingChain(HtmlUtils.htmlEscape(sortingChain.substring(0,
				sortingChain.length() - END_SEPARATOR_PLACEHOLDER.length())));
			String milestoneLabels = evaluateExpressionToString(array[21]).replace(",", MILESTONE_SEPARATOR);
			testcase.setMilestoneLabels(milestoneLabels);
			String nodeIds = evaluateExpressionToString(array[22]);
			String[] nodesIds = nodeIds.split(SEPARATOR_PLACEHOLDER);
			List<Long> hierachyNodes = new ArrayList<>();
			for(String node: nodesIds) {
				hierachyNodes.add(Long.parseLong(node.replace(END_SEPARATOR_PLACEHOLDER, "")));
			}
			testcase.setNodeIds(hierachyNodes);
			String script = evaluateExpressionToString(array[23]);
			script = script.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll(" ", "&nbsp;").replaceAll("\\t", "&nbsp;&nbsp;").replaceAll("\\n", "<br/>");
			testcase.setScript(script);
			testcase.setAllowAutomationWorkflow(evaluateExpressionToLong(array[24]));
			testcase.setAutomatable(evaluateExpressionToString(array[25]));
			testcase.setRequestStatus(evaluateExpressionToString(array[26]));
			testcase.setAutomationPriority(evaluateExpressionToString(array[27]));
			testcase.setPrintSteps(printSteps);

			result.add(testcase);

		}
		return result;
	}

	Collection<NodeBean> toNodesBean(Collection<Object[]> nodesData) {
		Collection<NodeBean> nodeBeans = new ArrayList<>();
		for(Object[] array: nodesData) {
			NodeBean nodeBean = new NodeBean();
			nodeBean.setProjectId(evaluateExpressionToLong(array[0]));
			nodeBean.setProjectName(evaluateExpressionToString(array[1]));
			nodeBean.setItemId(evaluateExpressionToLong(array[2]));
			nodeBeans.add(nodeBean);
		}
		return nodeBeans;
	}

	Collection<TestCaseStepsBean> toCallStepBean(Collection<Object[]> objects, Long idTestCase) {
		Collection<TestCaseStepsBean> result = new ArrayList<>();
		for (Object[] array: objects) {
			TestCaseStepsBean testCaseStepsBean = new TestCaseStepsBean();
			testCaseStepsBean.setId(evaluateExpressionToLong(array[0]));
			testCaseStepsBean.setAction(evaluateExpressionToString(array[1]));
			testCaseStepsBean.setExpectedResult(evaluateExpressionToString(array[2]));
			testCaseStepsBean.setType(evaluateExpressionToString(array[3]));
			testCaseStepsBean.setOrder(evaluateExpressionToLong(array[4]));
			testCaseStepsBean.setDataset(evaluateExpressionToString(array[5]));
			testCaseStepsBean.setTestCaseId(idTestCase);
			result.add(testCaseStepsBean);
		}
		return result;
	}

	Collection<Parameter> toParameter(Collection<Object[]> objects) {
		Collection<Parameter> parameters = new ArrayList<>();
		for(Object[] array: objects) {
			Parameter parameter = new Parameter();
			parameter.setName(evaluateExpressionToString(array[0]));
			parameter.setDescription(evaluateExpressionToString(array[1]));
			parameter.setTestCaseId(evaluateExpressionToLong(array[2]));
			parameter.setTestCaseSourceName(evaluateExpressionToString(array[3]));
			parameter.setTestCaseSourceRef(evaluateExpressionToString(array[4]));
			parameter.setTestCaseSourceProjectName(evaluateExpressionToString(array[5]));
			parameters.add(parameter);
		}
		return parameters;
	}

	Collection<Dataset> toDataSet(Collection<Object[]> objects) {
		Collection<Dataset> datasets = new ArrayList<>();
		for(Object[] array: objects) {
			Dataset dataset = new Dataset();
			dataset.setId(evaluateExpressionToLong(array[0]));
			dataset.setName(evaluateExpressionToString(array[1]));
			dataset.setTestCaseId(evaluateExpressionToLong(array[2]));
			datasets.add(dataset);
		}
		return datasets;
	}

	Collection<DatasetParamValue> toDataSetParamValue(Collection<Object[]> objects) {
		Collection<DatasetParamValue> datasetParamValues = new ArrayList<>();
		for(Object[] array: objects) {
			DatasetParamValue datasetParamValue = new DatasetParamValue();
			datasetParamValue.setDataSetId(evaluateExpressionToLong(array[0]));
			datasetParamValue.setName(evaluateExpressionToString(array[1]));
			datasetParamValue.setValue(evaluateExpressionToString(array[2]));
			datasetParamValues.add(datasetParamValue);
		}
		return datasetParamValues;
	}

	public void bindAll(Collection<TestCaseStepsBean> testCaseStepsBeans, Map<String, Collection<CufBean>> testCaseCufMap,
						Map<String, Collection<CufBean>> stepCufMap, Collection<LinkedRequirementsBean> linkedRequirementsBeans,
						Collection<TestCaseBean> testCaseBeans, Collection<Parameter> parameters, Collection<Dataset> datasets,
						Collection<DatasetParamValue> datasetParamValues, Collection<NodeBean> nodes) {
		tcIdsAlreadyAdd = new ArrayList<>();
		paragraphs = new ArrayList<>();
		projectId = null;
		if(!testCaseStepsBeans.isEmpty()) {
			bindCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.CUFS.getValue()));
			bindNumCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.NUM_CUFS.getValue()));
			bindTagCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.TAG_CUFS.getValue()));
			bindRtfCufToStep(testCaseStepsBeans, stepCufMap.get(CufType.RTF_CUFS.getValue()));
			bindTestStepToTestCase(testCaseBeans, testCaseStepsBeans);
		}
		bindParamValuesToDataset(datasets, datasetParamValues);
		bindDatasetToTestCase(datasets, testCaseBeans);
		bindCufToTestCase(testCaseBeans, testCaseCufMap.get(CufType.CUFS.getValue()));
		bindNumCufToTestCase(testCaseBeans, testCaseCufMap.get(CufType.NUM_CUFS.getValue()));
		bindTagCufTestCase(testCaseBeans, testCaseCufMap.get(CufType.TAG_CUFS.getValue()));
		bindRtfCufToTestCase(testCaseBeans, testCaseCufMap.get(CufType.RTF_CUFS.getValue()));
		bindParameterForTestCase(testCaseBeans, parameters);
		if(!linkedRequirementsBeans.isEmpty()) {
			bindLinkedReqToTc(testCaseBeans, linkedRequirementsBeans);
		}

		bindTestCaseToNode(testCaseBeans, nodes);

	}

	private void bindTestCaseToNode(Collection<TestCaseBean> testCaseBeans, Collection<NodeBean> nodeBeans) {
		for(NodeBean nodeBean: nodeBeans) {
			List<TestCaseBean> testCaseBeans1 = groupTestCaseForNode(testCaseBeans,nodeBean);
			nodeBean.setTestCasesBeans(testCaseBeans1);
		}
	}

	private void bindTestStepToTestCase(Collection<TestCaseBean> testCaseBeans, Collection<TestCaseStepsBean> testCaseStepsBeans) {
		for(TestCaseBean testCaseBean: testCaseBeans) {
			List<TestCaseStepsBean> testCaseStepBeanList = groupTestStepForTestCase(testCaseStepsBeans, testCaseBean);
			testCaseBean.setTcStepsBeans(testCaseStepBeanList);
		}
	}

	private void bindLinkedReqToTc(Collection<TestCaseBean> testCaseBeans, Collection<LinkedRequirementsBean>linkedRequirementsBeans) {
		for(TestCaseBean testCaseBean: testCaseBeans) {
			List<LinkedRequirementsBean> linkedRequirementsBeanList = groupLinkedRequirementForTestCase(linkedRequirementsBeans, testCaseBean);
			testCaseBean.setLinkedRequirements(linkedRequirementsBeanList);
		}
	}


	private void bindCufToStep(Collection<TestCaseStepsBean> testCaseStepsBeans,
							   Collection<CufBean> cufBeans) {
		for (TestCaseStepsBean testCaseStepsBean: testCaseStepsBeans) {
			List<CufBean> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setCufs(cufs);
		}

	}

	private void bindNumCufToStep(Collection<TestCaseStepsBean> testCaseStepsBeans,
							   Collection<CufBean> cufBeans) {
		for (TestCaseStepsBean testCaseStepsBean: testCaseStepsBeans) {
			List<CufBean> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setNumCufs(cufs);
		}

	}

	private void bindTagCufToStep(Collection<TestCaseStepsBean> testCaseStepsBeans,
							   Collection<CufBean> cufBeans) {
		for (TestCaseStepsBean testCaseStepsBean: testCaseStepsBeans) {
			List<CufBean> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setTagCufs(cufs);
		}

	}

	private void bindRtfCufToStep(Collection<TestCaseStepsBean> testCaseStepsBeans,
							   Collection<CufBean> cufBeans) {
		for (TestCaseStepsBean testCaseStepsBean: testCaseStepsBeans) {
			List<CufBean> cufs = groupCufsForTestSteps(cufBeans, testCaseStepsBean);
			testCaseStepsBean.setRtfCufs(cufs);
		}

	}

	private void bindParameterForTestCase(Collection<TestCaseBean> testCases, Collection<Parameter> parameters) {
		for (TestCaseBean testCase: testCases) {
			List<Parameter> params = groupParameterForTestCase(parameters, testCase);
			testCase.setParameters(params);
		}
	}

	private void bindParamValuesToDataset(Collection<Dataset> datasets, Collection<DatasetParamValue> datasetParamValues) {
		for(Dataset dataset: datasets) {
			List<DatasetParamValue> paramValues = groupParamValueToDataset(datasetParamValues, dataset);
			dataset.setDatasetParamValues(paramValues);
		}
	}

	private void bindDatasetToTestCase(Collection<Dataset> datasets, Collection<TestCaseBean> testCases) {
		for(TestCaseBean testCase: testCases) {
			List<Dataset> datasetList = groupDatasetToTestCase(datasets, testCase);
			testCase.setDatasets(datasetList);
		}
	}

	private void bindCufToTestCase(Collection<TestCaseBean> testCaseBeans,
							   Collection<CufBean> cufBeans) {
		for (TestCaseBean testCaseBean: testCaseBeans) {
			List<CufBean> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setCufs(cufs);
		}

	}

	private void bindNumCufToTestCase(Collection<TestCaseBean> testCaseBeans,
								  Collection<CufBean> cufBeans) {
		for (TestCaseBean testCaseBean: testCaseBeans) {
			List<CufBean> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setNumCufs(cufs);
		}

	}

	private void bindTagCufTestCase(Collection<TestCaseBean> testCaseBeans,
								  Collection<CufBean> cufBeans) {
		for (TestCaseBean testCaseBean: testCaseBeans) {
			List<CufBean> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setTagCufs(cufs);
		}

	}

	private void bindRtfCufToTestCase(Collection<TestCaseBean> testCaseBeans,
								  Collection<CufBean> cufBeans) {
		for (TestCaseBean testCaseBean: testCaseBeans) {
			List<CufBean> cufs = groupCufsForTestCases(cufBeans, testCaseBean);
			testCaseBean.setRtfCufs(cufs);
		}

	}

	public List<CufBean> groupCufsForTestCases(Collection<CufBean> cufData, TestCaseBean testCaseBean) {
		List<CufBean> cufs = new ArrayList<>();
		for (CufBean cufBean: cufData) {
			if (testCaseBean.acceptAsCuf(cufBean)) {
				cufs.add(cufBean);
			}
		}
		return cufs;
	}

	public List<TestCaseBean> groupTestCaseForNode(Collection<TestCaseBean> testCaseBeans, NodeBean nodeBean) {
		Iterator<TestCaseBean> iterator = testCaseBeans.iterator();
		List<TestCaseBean> tc = new ArrayList<>();
		Set<Long> tcIds = new HashSet<>();
		while (iterator.hasNext()) {

			TestCaseBean next = iterator.next();
			if(next.getNodeIds().contains(nodeBean.getItemId())) {
				tcIds.addAll(next.getNodeIds());
			}
		}

		Iterator<TestCaseBean> iterator1 = testCaseBeans.iterator();
		while(iterator1.hasNext()) {
			TestCaseBean testCaseBean = iterator1.next();
			if(tcIds.contains(testCaseBean.getId()) && !tcIdsAlreadyAdd.contains(testCaseBean.getId())) {
				if(testCaseBean.isAFolder()) {
					changeParagraph(testCaseBean, nodeBean);
				}
				testCaseBean.setParagraph(paragraphs);
				tc.add(testCaseBean);
				tcIdsAlreadyAdd.add(testCaseBean.getId());
			}
		}
		return tc;
	}

	public List<Parameter> groupParameterForTestCase(Collection<Parameter> parameters, TestCaseBean testCase) {
		List<Parameter> params = new ArrayList<>();
		for(Parameter param: parameters) {
			if(testCase.getId().equals(param.getTestCaseId())) {
				params.add(param);
			}
		}
		return params;
	}

	private List<DatasetParamValue> groupParamValueToDataset(Collection<DatasetParamValue> datasetParamValues, Dataset dataset) {
		List<DatasetParamValue> result = new ArrayList<>();
		for(DatasetParamValue datasetParamValue : datasetParamValues) {
			if(dataset.getId().equals(datasetParamValue.getDataSetId())) {
				result.add(datasetParamValue);
			}
		}
		return result;
	}

	private List<Dataset> groupDatasetToTestCase(Collection<Dataset> datasets, TestCaseBean testCase) {
		List<Dataset> result = new ArrayList<>();
		for(Dataset dataset : datasets) {
			if(dataset.getTestCaseId().equals(testCase.getId())) {
				result.add(dataset);
			}
		}
		return result;
	}

	private void changeParagraph(TestCaseBean testCaseBean, NodeBean nodeBean) {
		int index;
		int value;

		/**
		 * Control if the current node is in same project of previous node.
		 */
		if(projectId != null && !projectId.equals(nodeBean.getProjectId())) {
			paragraphs = new ArrayList<>();
		}

		projectId = nodeBean.getProjectId();

		if(testCaseBean.getLevel()+POSITION > paragraphs.size()) {
			paragraphs.add(POSITION);
		} else if (testCaseBean.getLevel()+POSITION == paragraphs.size()) {
			index = testCaseBean.getLevel().intValue();
			value = paragraphs.get(index)+POSITION;
			paragraphs.set(index, value);
		} else if(testCaseBean.getLevel()+POSITION < paragraphs.size()) {
			index = testCaseBean.getLevel().intValue();
			value = paragraphs.get(index)+POSITION;
			paragraphs.set(index, value);
			paragraphs.subList(index+POSITION, paragraphs.size()).clear();
		}

	}

	private List<TestCaseStepsBean> groupTestStepForTestCase(Collection<TestCaseStepsBean> testCaseStepsBeans, TestCaseBean testCaseBean) {
		List<TestCaseStepsBean> tc = new ArrayList<>();
		for(TestCaseStepsBean testCaseStepsBean: testCaseStepsBeans) {
			if(testCaseBean.getId().equals(testCaseStepsBean.getTestCaseId())) {
				tc.add(testCaseStepsBean);
			}
		}
		return tc;
	}

	private List<LinkedRequirementsBean> groupLinkedRequirementForTestCase(Collection<LinkedRequirementsBean> linkedRequirementsBeans, TestCaseBean testCaseBean) {
		List<LinkedRequirementsBean> linkedRequirementsBeanArrayList = new ArrayList<>();
		for(LinkedRequirementsBean linkedRequirementsBean : linkedRequirementsBeans) {
			if(testCaseBean.getId().equals(linkedRequirementsBean.getTestCaseId())) {
				linkedRequirementsBeanArrayList.add(linkedRequirementsBean);

			}
		}
		return linkedRequirementsBeanArrayList;
	}

	private List<CufBean> groupCufsForTestSteps(Collection<CufBean> cufData, TestCaseStepsBean testCaseStepsBean) {
		List<CufBean> cufs = new ArrayList<>();
		for(CufBean cuf : cufData) {
			if (testCaseStepsBean.acceptAsCuf(cuf)) {
				cufs.add(cuf);
			}
		}
		return cufs;
	}

	private Locale currentLocale() {
		Locale current = LocaleContextHolder.getLocale();

		if (current == null) {
			current = Locale.getDefault();
		}

		return current;
	}

	private String convertDatetoString(Date date) {

		return DateFormat.getDateTimeInstance(DateFormat.SHORT,
			DateFormat.SHORT, currentLocale()).format(date);

	}
	private String evaluateExpressionToString(Object obj) {

		if (obj instanceof Date){
			return obj != null ? convertDatetoString((Date) obj): "";
		}

		return obj != null ? obj.toString() : "";

	}

	protected Long evaluateExpressionToLong(Object obj) {

		return Long.valueOf(obj.toString());

	}

	protected Boolean evaluateExpressionToBoolean(Object obj) {

		return Boolean.parseBoolean(obj.toString());
	}

}
