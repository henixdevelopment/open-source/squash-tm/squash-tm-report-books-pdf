/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.beans;

import java.util.ArrayList;
import java.util.List;

public class TestCaseStepsBean {

	private Long id;

	private String action;

	private String expectedResult;

	private String type;

	private Long order;

	private String dataset;

	private String prerequisites;

	private Long testCaseId;

	private Boolean delegateParameterValues;

	private List<CufBean> rtfCufs;
	private List<CufBean> cufs;
	private List<CufBean> tagCufs;
	private List<CufBean> numCufs;

	public List<CufBean> getTagCufs() {
		return tagCufs;
	}

	public void setTagCufs(List<CufBean> tagcufs) {
		this.tagCufs = tagcufs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setOrder(Long order) {
		this.order = order;
	}

	public Long getOrder() {
		return order;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getPrerequisites() {
		return prerequisites;
	}

	public void setPrerequisites(String prerequisites) {
		this.prerequisites = prerequisites;
	}

	public void setCufs(List<CufBean> cufs) {
		this.cufs = cufs;
	}

	public void setRtfCufs(List<CufBean> cufs) {
		this.rtfCufs = cufs;
	}

	public List<CufBean> getCufs() {
		return cufs;
	}

	public List<CufBean> getRtfCufs() {
		List<CufBean> list = new ArrayList<CufBean>(rtfCufs);
		list.addAll(getTagCufs());
		return list;
	}

	public List<CufBean> getNumCufs() {
		return numCufs;
	}

	public void setNumCufs(List<CufBean> numCufs) {
		this.numCufs = numCufs;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public boolean acceptAsCuf(CufBean cufBean) {
		return  cufBean.getEntityId().equals(id);
	}

	public Boolean getDelegateParameterValues() { return delegateParameterValues; }

	public void setDelegateParameterValues(Boolean delegateParameterValues) { this.delegateParameterValues = delegateParameterValues; }
}
