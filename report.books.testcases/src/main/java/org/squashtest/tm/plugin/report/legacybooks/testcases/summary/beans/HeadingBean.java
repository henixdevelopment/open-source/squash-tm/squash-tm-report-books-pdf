/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.summary.beans;

public class HeadingBean
{


	/**
	 *
	 */
	private String headingText = null;
	private String reference = null;
	private Integer pageIndex = null;
	private Long type = null;


	/**
	 *
	 */
	public HeadingBean(
		String text,
		String reference,
		Integer pageIndex,
		Long type
		)
	{
		this.headingText = text;
		this.reference = reference;
		this.pageIndex = pageIndex;
		this.type = type;
	}


	/**
	 *
	 */
	public String getHeadingText()
	{
		return this.headingText;
	}


	/**
	 *
	 */
	public String getReference()
	{
		return this.reference;
	}


	/**
	 *
	 */
	public Integer getPageIndex()
	{
		return this.pageIndex;
	}


	/**
	*
	*/
	public void setPageIndex(Integer pageIndex)
	{
		this.pageIndex = pageIndex;
	}

	/**
	 *
	 */
	public Long getType()
	{
		return this.type;
	}

}
