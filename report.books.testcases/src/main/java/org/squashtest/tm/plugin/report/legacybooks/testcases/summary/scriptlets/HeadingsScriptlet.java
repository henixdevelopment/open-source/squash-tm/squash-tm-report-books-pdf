/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.summary.scriptlets;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.plugin.report.legacybooks.testcases.summary.beans.HeadingBean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

public class HeadingsScriptlet extends JRDefaultScriptlet {

	private static final String CHAPTER_NAME_HEADER_VARIABLE = "ChapterName";
	private static final String CHAPTER_NAME_2_HEADER_VARIABLE = "ChapterName2";
	/**
	 *
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	public Boolean addHeading(String headerVariable) throws JRScriptletException {
		Collection headings = (Collection) this.getVariableValue("HeadingsCollection");

		String text;
		String reference;
		Long type;

		Integer pageIndex = (Integer) this.getVariableValue("MY_PAGE_NUMBER");

		text = "" + HtmlUtils.htmlUnescape((String) this.getVariableValue(headerVariable));

		if(headerVariable.equals(CHAPTER_NAME_2_HEADER_VARIABLE)){
			reference = headerVariable + "_" + this.getVariableValue(CHAPTER_NAME_HEADER_VARIABLE) + "_" + this.getVariableValue(headerVariable);
		} else {
			reference = headerVariable + "_" + this.getVariableValue(headerVariable);
		}

		type = (Long) this.getFieldValue("folder");
		headings.add(new HeadingBean(text, reference, pageIndex, type));

		return Boolean.TRUE;
	}

	@SuppressWarnings("unchecked")
	public Boolean addProjectName(String projectNameField) throws JRScriptletException {
		@SuppressWarnings("rawtypes")
		Collection headings = (Collection) this.getVariableValue("HeadingsCollection");
		//Issue 5034. If headings are null jasper don't thrown any error but don't show project name...
		headings = headings == null ? new ArrayList<Object>() : headings;

		String text;
		String reference;
		Long type;

		Integer pageIndex = (Integer) this.getVariableValue("PAGE_NUMBER");
		text = "" + HtmlUtils.htmlUnescape((String) this.getFieldValue(projectNameField));
		reference = projectNameField + "_" + this.getFieldValue(projectNameField);
		type = 2L;
		headings.add(new HeadingBean(text, reference, pageIndex, type));

		return Boolean.TRUE;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public Boolean buildToc() throws JRScriptletException {
		Collection headings = (Collection) this.getVariableValue("HeadingsCollection");
		ArrayList toc = (ArrayList) this.getVariableValue("TOC");
		if (headings != null) {
			toc.addAll(headings);
		}
		return Boolean.TRUE;
	}

	public Locale currentLocale() {
		Locale current = LocaleContextHolder.getLocale();

		if (current == null) {
			current = Locale.getDefault();
		}

		return current;
	}

}
