/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.testcases.beans;

import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestCaseBean {

	private Long id;

	private String name;

	private String description;

	private String prerequisites;

	private Long level;

	private String createdBy;

	private String createdOn;

	private String lastModifiedBy;

	private String lastModifiedOn;

	private String paragraph;

	private String reference;

	private String importance;

	private String nature;

	private String type;

	private String natureType;

	private String typeType;

	private String status;

	private Long allowAutomationWorkflow;

	private String automatable;

	private String requestStatus;

	private String automationPriority;

	private Long executionMode;

	private Long folder;

	private String chain;

	private Long attachments;

	/**
	 * the sorting chain ensures that within a folder all tests cases will be ranked before all folders, and that each
	 * sort of bean will be sorted using reference+name among themselves. See the awfull SQL to see how it's done.
	 */
	private String sortingChain;

	private String script;

	private List<TestCaseStepsBean> tcStepsBeans;

	private List<LinkedRequirementsBean> linkedRequirements;

	private List<CufPair> cufPairs = new ArrayList<CufPair>();

	private List<CufBean> rtfCufs = new ArrayList<CufBean>();

	private List<CufBean> numCufs = new ArrayList<CufBean>();

	private List<CufBean> tagCufs = new ArrayList<CufBean>();

	private String milestoneLabels;

	private List<Long> nodeIds;

	private List<Parameter> parameters = new ArrayList<>();

	private List<Dataset> datasets = new ArrayList<>();
	private Boolean printSteps;

	public String getNatureType() {
		return natureType;
	}

	public void setNatureType(String natureType) {
		this.natureType = natureType;
	}

	public String getTypeType() {
		return typeType;
	}

	public void setTypeType(String typeType) {
		this.typeType = typeType;
	}

	public List<CufBean> getTagCufs() {
		return tagCufs;
	}

	public void setTagCufs(List<CufBean> tagCufs) {
		this.tagCufs = tagCufs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getUnescapedName() {
		return HtmlUtils.htmlUnescape(name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public String getParagraph() {
		return paragraph;
	}

	public void setParagraph(List<Integer> paragraphLevel) {

		paragraph = "";

		if (paragraphLevel != null && paragraphLevel.size() > 0) {
			Iterator<Integer> itr = paragraphLevel.iterator();
			String delimiter = "";
			while (itr.hasNext()) {
				paragraph = paragraph + delimiter + itr.next();
				delimiter = ".";
			}
		}
	}

	public String getReference() {
		return reference;
	}

	public String getUnescapedReference() {
		return HtmlUtils.htmlUnescape(reference);
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getNature() {
		return nature.toLowerCase();
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getType() {
		return type.toLowerCase();
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getAllowAutomationWorkflow() {
		return allowAutomationWorkflow;
	}

	public void setAllowAutomationWorkflow(Long allowAutomationWorkflow) {
		this.allowAutomationWorkflow = allowAutomationWorkflow;
	}

	public String getAutomatable() {
		return automatable;
	}

	public void setAutomatable(String automatable) {
		this.automatable = automatable;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getAutomationPriority() {
		return automationPriority;
	}

	public void setAutomationPriority(String automationPriority) {
		this.automationPriority = automationPriority;
	}

	public void setExecutionMode(Long executionMode) {
		this.executionMode = executionMode;
	}

	public Long getExecutionMode() {
		return executionMode;
	}

	public Long getFolder() {
		return folder;
	}

	public void setFolder(Long folder) {
		this.folder = folder;
	}

	public String getChain() {
		return chain;
	}

	public String getUnescapedChain() {
		return HtmlUtils.htmlUnescape(chain);
	}

	public void setChain(String chain) {
		this.chain = chain;
	}

	public void setPrerequisites(String prerequisites) {
		this.prerequisites = prerequisites;
	}

	public String getPrerequisites() {
		return prerequisites;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setTcStepsBeans(List<TestCaseStepsBean> tcStepsBeans) {
		this.tcStepsBeans = tcStepsBeans;
	}

	public List<TestCaseStepsBean> getTcStepsBeans() {
		return tcStepsBeans;
	}

	public void setLinkedRequirements(List<LinkedRequirementsBean> linkedRequirements) {
		this.linkedRequirements = linkedRequirements;
	}

	public List<LinkedRequirementsBean> getLinkedRequirements() {
		return linkedRequirements;
	}

	public List<CufPair> getCufPairs() {
		return cufPairs;
	}

	public void setCufs(List<CufBean> cufs) {
		this.cufPairs.clear();
		Iterator<CufBean> it = cufs.iterator();

		while (it.hasNext()) {
			CufBean next = it.next();
			CufPair pair = new CufPair();
			pair.setFirstCuf(next);
			if (it.hasNext()) {
				pair.setSecondCuf(it.next());
			}
			this.cufPairs.add(pair);
		}
	}

	public void setRtfCufs(List<CufBean> cufs) {
		this.rtfCufs = cufs;

	}

	public List<CufBean> getRtfCufs() {
		List<CufBean> list = new ArrayList<CufBean>(rtfCufs);
		list.addAll(getTagCufs());
		return list;
	}

	public List<CufBean> getNumCufs() {
		return numCufs;
	}

	public void setNumCufs(List<CufBean> numCufs) {
		this.numCufs = numCufs;
	}

	public Long getAttachments() {
		return attachments;
	}

	public void setAttachments(Long attachments) {
		this.attachments = attachments;
	}

	public String getSortingChain() {
		return sortingChain;
	}

	public void setSortingChain(String sortingChain) {
		this.sortingChain = sortingChain;
	}

	public boolean isAFolder() {
		return (folder == 1l);
	}

	public String getMilestoneLabels() {
		return milestoneLabels;
	}

	public void setMilestoneLabels(String milestoneLabels) {
		this.milestoneLabels = milestoneLabels;
	}

	public boolean acceptAsCuf(CufBean cufBean) {
		return  cufBean.getEntityId().equals(id);
	}

	public List<Long> getNodeIds() {
		return nodeIds;
	}

	public void setNodeIds(List<Long> nodeIds) {
		this.nodeIds = nodeIds;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public List<Dataset> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<Dataset> datasets) {
		this.datasets = datasets;
	}

	public Boolean getPrintSteps() {
		return printSteps;
	}

	public void setPrintSteps(Boolean printSteps) {
		this.printSteps = printSteps;
	}
}
