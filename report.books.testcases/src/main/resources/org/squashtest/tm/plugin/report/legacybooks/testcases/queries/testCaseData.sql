
-- comments
--
-- 1/ chain_id is now a placeholder for a data that isn't used anymore,
-- 2/ same goes for node_type
-- 3/ we test that a given node is a folder or a test case by testing if it's tc_type is null (that cannot happen for regular test cases)
-- 4/ use MAX(value) to bluff postgresql when he complains about "value must be in group by or blabla"
-- 5/ count attachment in a sub query to avoid join with attachment table as it messes with chain and sorting_key values
-- 6/ Same as above: group concat on milestone labels in sub query to avoid join with MILESTONE and MILESTONE_TEST_CASE tables
-- because it messes with chain and sorting_key values
select
	group_concat(concat(COALESCE(tc_clos.REFERENCE, ''), tcln_clos.NAME, '=Sep=') order by clos.DEPTH desc) as chain,
	tcln.tcln_id as id,
	MAX((case when tc.tc_type is NULL then 1 else 0 end)) as folder,
	tcln.name as name,
	max(clos.depth) as level,
	MAX(COALESCE(tc.importance, 'UNDEFINED')) as importance,
	MAX(COALESCE(tc_nature.label, 'UNDEFINED')) as tcnature,
	MAX(COALESCE(tc_type.label, 'UNDEFINED')) as tctype,
	MAX(COALESCE(tc_nature.item_type, 'UNDEFINED')) as tcnatureType,
	MAX(COALESCE(tc_type.item_type, 'UNDEFINED')) as tctypeType,
	MAX(COALESCE(tc.tc_status, 'WORK_IN_PROGRESS')) as tcstatus,
	MAX((case when tc.ta_test is NULL then 0 else 1 end)) as execution_mode,
	MAX(COALESCE(tc.prerequisite, ' ')) as prerequisites,
	MAX(COALESCE(tc.reference, ' ')) as reference,
	tcln.created_on as created_on,
	tcln.created_by as created_by,
	tcln.last_modified_on as last_modified_on,
	tcln.last_modified_by as last_modified_by,
	tcln.description as description,
	(select count(distinct attach.attachment_id) from ATTACHMENT attach where attach.attachment_list_id = tcln.attachment_list_id) as attachments,
	group_concat(concat((case when tc_clos.tc_type is NULL then '1' else '0' end), COALESCE(tc_clos.reference, ' '), tcln_clos.name, '=Sep=') order by clos.depth desc) as sorting_key,
	case when MAX(tc.tc_type) is NULL then '' else
		(
			select group_concat(distinct mstones.LABEL)
			from MILESTONE mstones
					 left join MILESTONE_TEST_CASE tcstones on mstones.MILESTONE_ID = tcstones.MILESTONE_ID
					 left join TEST_CASE tc on tcln.tcln_id = tcstones.TEST_CASE_ID
			where mstones.MILESTONE_ID = tcstones.MILESTONE_ID and tcln.tcln_id = tcstones.TEST_CASE_ID
		) end as milestones,
	group_concat(concat((select distinct tcln_clos.TCLN_ID),'=Sep=') order by clos.depth desc) as nodeid,
	stc.SCRIPT as SCRIPT,
	MAX((case when p.allow_automation_workflow is false then 0 else 1 end)) as allow_automation_workflow,
	tc.automatable as automatable,
	ar.request_status as request_status,
	ar.automation_priority as automation_priority
from TCLN_RELATIONSHIP_CLOSURE clos
inner join TEST_CASE_LIBRARY_NODE tcln_clos on clos.ancestor_id=tcln_clos.tcln_id
left join TEST_CASE tc_clos on tcln_clos.tcln_id = tc_clos.tcln_id
inner join TEST_CASE_LIBRARY_NODE tcln on clos.descendant_id=tcln.tcln_id
left join TEST_CASE tc on tcln.tcln_id = tc.tcln_id
left join INFO_LIST_ITEM tc_nature on tc.tc_nature = tc_nature.item_id
left join INFO_LIST_ITEM tc_type on tc.tc_type = tc_type.item_id
left join SCRIPTED_TEST_CASE stc on tc.TCLN_ID = stc.TCLN_ID
left join PROJECT p on tcln.project_id = p.project_id
left join AUTOMATION_REQUEST ar on tc.automation_request_id = ar.automation_request_id
where clos.descendant_id in (select clos1.ancestor_id from TCLN_RELATIONSHIP_CLOSURE clos1 where clos1.descendant_id in (:testcaseIds))
or clos.descendant_id in (select clos2.descendant_id from TCLN_RELATIONSHIP_CLOSURE clos2 where clos2.ancestor_id in (:testcaseIds))
group by clos.descendant_id, tcln.tcln_id, stc.script, tc.automatable, ar.request_status, ar.automation_priority, tc.TCLN_ID
order by sorting_key
