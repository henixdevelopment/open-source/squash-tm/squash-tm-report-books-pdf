SELECT *
FROM  (SELECT tcs.STEP_ID         AS ID,
			  ats.ACTION          AS ACTION,
			  ats.EXPECTED_RESULT AS EXPECTED_RESULT,
			  'S'                 AS TYPE,
			  tcs.STEP_ORDER      AS STEP_ORDER,
			  NULL                AS STEP_DATASET,
			  tcs.TEST_CASE_ID    AS TEST_CASE_ID,
			  'false'             AS DELEGATE_PARAMETER_VALUES
	   FROM   TEST_CASE_STEPS tcs
				  JOIN ACTION_TEST_STEP ats
					   ON tcs.STEP_ID = ats.TEST_STEP_ID
	   WHERE  tcs.TEST_CASE_ID IN ( :testcaseIds )
	   UNION
	   SELECT cts.CALLED_TEST_CASE_ID                                                      AS ID,
			  tcln.NAME                                                                    AS ACTION,
			  ' '                                                                          AS EXPECTED_RESULT,
			  'C'                                                                          AS TYPE,
			  tcs.STEP_ORDER                                                               AS STEP_ORDER,
              ds.NAME                                                                      AS STEP_DATASET,
			  tcs.TEST_CASE_ID                                                             AS TEST_CASE_ID,
			  CASE WHEN cts.DELEGATE_PARAMETER_VALUES IS TRUE THEN 'true' ELSE 'false' end AS DELEGATE_PARAMETER_VALUES
	   FROM   TEST_CASE_STEPS tcs
				  JOIN CALL_TEST_STEP cts
					   ON cts.TEST_STEP_ID = tcs.STEP_ID
				  JOIN TEST_CASE_LIBRARY_NODE tcln
					   ON tcln.TCLN_ID = cts.CALLED_TEST_CASE_ID
				  LEFT JOIN DATASET ds
							ON cts.CALLED_DATASET = ds.DATASET_ID
	   WHERE  tcs.TEST_CASE_ID IN ( :testcaseIds )) sub
ORDER  BY sub.STEP_ORDER
