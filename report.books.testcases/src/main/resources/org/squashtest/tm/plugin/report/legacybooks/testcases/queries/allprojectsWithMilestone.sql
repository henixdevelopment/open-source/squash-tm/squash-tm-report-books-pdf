select MAX(TCLN.PROJECT_ID) as PROJECT_ID,
	  MAX(P.NAME) as PROJECT_NAME,
	  TCLN.TCLN_ID as ITEM_ID,
	MAX(SORT.SORTING_NAME) as SORTING_CHAIN
from TEST_CASE_LIBRARY_NODE TCLN
join TEST_CASE_LIBRARY_CONTENT TCLC on TCLC.CONTENT_ID = TCLN.TCLN_ID
join PROJECT P on P.PROJECT_ID=TCLN.PROJECT_ID
join MILESTONE_BINDING mbinding on mbinding.PROJECT_ID = P.PROJECT_ID
join MILESTONE m on m.MILESTONE_ID = mbinding.MILESTONE_ID
join (
	-- the sorting key for each node is <isfolder><reference><name> ensuring that folders
	-- are ranked after the test cases
	select tclnrew.TCLN_ID, CONCAT(CASE WHEN tcrew.TCLN_ID IS NULL THEN '1' ELSE '0' END , COALESCE(tcrew.REFERENCE , ' ') , tclnrew.NAME ) as SORTING_NAME
	from TEST_CASE_LIBRARY_NODE tclnrew left outer join  TEST_CASE tcrew on tclnrew.TCLN_ID = tcrew.TCLN_ID

) SORT on SORT.TCLN_ID = TCLC.CONTENT_ID
where P.PROJECT_TYPE = 'P'
and m.MILESTONE_ID in (:milestones)
and TCLN.PROJECT_ID in (:projectIds)
group by TCLN.TCLN_ID
order by PROJECT_NAME ASC, SORTING_CHAIN ASC
