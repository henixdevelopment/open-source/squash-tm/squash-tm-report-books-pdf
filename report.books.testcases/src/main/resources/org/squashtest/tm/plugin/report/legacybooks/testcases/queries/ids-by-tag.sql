select distinct TC.TCLN_ID
from TEST_CASE TC
join TEST_CASE_LIBRARY_NODE TCLN on TC.TCLN_ID = TCLN.TCLN_ID
join CUSTOM_FIELD_VALUE cfv on cfv.bound_entity_id = TC.TCLN_ID
join CUSTOM_FIELD_VALUE_OPTION cfvo on cfvo.cfv_id = cfv.cfv_id
where cfv.BOUND_ENTITY_TYPE = 'TEST_CASE'
and cfvo.LABEL in (:tags)
and cfv.FIELD_TYPE = 'TAG'
and TCLN.PROJECT_ID in (:projectIds)
