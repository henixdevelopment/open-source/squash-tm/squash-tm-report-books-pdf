SELECT group_concat(concat(cfvo.label,'=Sep=') order by cfvo.label asc),
cf.LABEL as label,
cf.INPUT_TYPE as inputType ,
cfv.BOUND_ENTITY_ID as boundId
FROM CUSTOM_FIELD_VALUE cfv
JOIN CUSTOM_FIELD_BINDING cfb ON cfv.CFB_ID = cfb.CFB_ID
JOIN CUSTOM_FIELD cf ON cf.CF_ID = cfb.CF_ID
JOIN REQUIREMENT_VERSION rv ON rv.RES_ID = cfv.BOUND_ENTITY_ID
JOIN CUSTOM_FIELD_VALUE_OPTION cfvo ON cfv.CFV_ID = cfvo.CFV_ID
WHERE cfv.BOUND_ENTITY_TYPE = 'REQUIREMENT_VERSION'
AND rv.RES_ID in (:versionIds) 
GROUP BY cfv.CFV_ID, cf.LABEL, cf.INPUT_TYPE, cfv.BOUND_ENTITY_TYPE, cfb.POSITION 
order by cfb.POSITION
