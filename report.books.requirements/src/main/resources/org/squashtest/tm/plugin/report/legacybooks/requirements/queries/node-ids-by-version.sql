select distinct RLN.RLN_ID
from REQUIREMENT_VERSION VERSION
		 inner join REQUIREMENT_LIBRARY_NODE RLN on VERSION.REQUIREMENT_ID = RLN.RLN_ID
where VERSION.RES_ID in (:versionIds)
