select distinct VERSION.RES_ID
from REQUIREMENT_VERSION VERSION
join REQUIREMENT_LIBRARY_NODE RLN on VERSION.REQUIREMENT_ID = RLN.RLN_ID
join CUSTOM_FIELD_VALUE cfv on cfv.bound_entity_id = VERSION.RES_ID
join CUSTOM_FIELD_VALUE_OPTION cfvo on cfvo.cfv_id = cfv.cfv_id
where cfv.BOUND_ENTITY_TYPE = 'REQUIREMENT_VERSION'
and cfvo.LABEL in (:tags)
and cfv.FIELD_TYPE = 'TAG'
and RLN.PROJECT_ID in (:projectIds)
