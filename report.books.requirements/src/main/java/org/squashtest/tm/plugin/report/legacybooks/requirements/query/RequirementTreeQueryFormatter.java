/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.query;

import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.AttachReq;
import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.CufData;
import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.FolderData;
import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.ProjectData;
import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.ReqLinkData;
import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.RequirementVersionData;
import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.TestCaseData;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RequirementTreeQueryFormatter {
	private static final String END_SEPARATOR_PLACEHOLDER = "=Sep=";
	private static final String TAG_SEPARATOR = "|";
	private static final String SEPARATOR_PLACEHOLDER = END_SEPARATOR_PLACEHOLDER + ",";

	private static final String REQLINK_RELATED ="requirement-version.link.type.related";
	private static final String REQLINK_PARENT ="requirement-version.link.type.parent";
	private static final String REQLINK_CHILD ="requirement-version.link.type.child";
	private static final String REQLINK_DUPLICATE ="requirement-version.link.type.duplicate";


	private RequirementTreeQueryFinder queryFinder;

	public void setQueryFinder(RequirementTreeQueryFinder queryFinder) {
		this.queryFinder = queryFinder;
	}
	// ************************ DTO making *****************************

	/**
	 * Accepts collections of tuples (id, name)
	 *
	 * @return
	 */
	Collection<ProjectData> toProjectData(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Collection<ProjectData> result = new ArrayList<ProjectData>(tuples.size());

		for (Object[] array : tuples) {

			ProjectData newData = new ProjectData();

			Long id = konvertor.from(array[0]).toLong();
			String name = konvertor.from(array[1]).toString();

			newData.setId(id);
			newData.setName(name);

			result.add(newData);

		}

		return result;

	}

	/**
	 * accepts collections of tuples(ancestor id, id, project id, name, description). the breadcrumb and index are not
	 * filled yet.
	 *
	 * Will also create root folders for each projects.
	 *
	 * @param tuples
	 * @return
	 */
	Collection<FolderData> toFolderData(Collection<ProjectData> projects, Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Collection<FolderData> result = new ArrayList<FolderData>();

		for (Object[] array : tuples) {

			FolderData newData = new FolderData();

			Long ancestorId = konvertor.from(array[0]).toLong();
			Long folderId = konvertor.from(array[1]).toLong();
			Long projectId = konvertor.from(array[2]).toLong();
			String name = konvertor.from(array[3]).toString();
			String description = konvertor.from(array[4]).toString();

			newData.setAncestorId(ancestorId);
			newData.setFolderId(folderId);
			newData.setProjectId(projectId);
			newData.setName(name);
			newData.setDescription(description);

			result.add(newData);

		}

		// now create the stub root folders

		for (ProjectData project : projects) {

			FolderData newData = new FolderData();
			newData.setProjectId(project.getId()); // and leave the rest null
			newData.setPseudoRoot(true);
			result.add(newData);
		}

		return result;

	}

	Collection<TestCaseData> toTestCaseData(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Collection<TestCaseData> result = new ArrayList<TestCaseData>();

		for (Object[] array : tuples) {

			TestCaseData data = new TestCaseData();

			String projectName = konvertor.from(array[0]).toString();
			String testCaseName = konvertor.from(array[1]).toString();
			String importance = konvertor.from(array[2]).toString();
			Long requirementVersionId = konvertor.from(array[3]).toLong();
			String reference = konvertor.from(array[4]).toString();

			data.setProjectName(projectName);
			data.setName(testCaseName);
			data.setImportance(importance);
			data.setRequirementVersionId(requirementVersionId);
			data.setReference(reference);

			result.add(data);
		}

		return result;

	}

	Collection<CufData> toCufData(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Collection<CufData> result = new ArrayList<CufData>();

		for (Object[] array : tuples) {

			CufData data = new CufData();

			String cufValue = konvertor.from(array[0]).toString();
			String cufLabel = konvertor.from(array[1]).toString();
			String cufType = konvertor.from(array[2]).toString();
			Long reqVersionId = konvertor.from(array[3]).toLong();

			data.setLabel(cufLabel);
			data.setReqVersionId(reqVersionId);
			data.setType(cufType);
			data.setValue(cufValue);

			result.add(data);
		}

		return result;

	}

	Collection<RequirementVersionData> toRequirementVersionData(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Collection<RequirementVersionData> result = new LinkedList<RequirementVersionData>();

		for (Object[] array : tuples) {

			RequirementVersionData newData = new RequirementVersionData();

			Long folderId = konvertor.from(array[0]).toLong();
			Long parentReqVersionId = konvertor.from(array[1]).toLong();
			Long projectId = konvertor.from(array[2]).toLong();
			Long versionId = konvertor.from(array[3]).toLong();
			Long requirementId = konvertor.from(array[4]).toLong();
			String reference = konvertor.from(array[5]).toString();
			int versionNumber = konvertor.from(array[6]).toInt();
			String criticality = konvertor.from(array[7]).toString();
			String status = konvertor.from(array[8]).toString();
			String category = konvertor.from(array[9]).toString();
			String categoryType = konvertor.from(array[10]).toString();
			int nbAttachs = konvertor.from(array[11]).toInt();
			String name = konvertor.from(array[12]).toString();
			String description = konvertor.from(array[13]).toString();
			String createdBy = konvertor.from(array[14]).toString();
			Date createdOn = konvertor.from(array[15]).toDate();
			String modifiedBy = konvertor.from(array[16]).toString();
			Date modifiedOn = konvertor.from(array[17]).toDate();
			Integer totalVersionNumber = konvertor.from(array[18]).toInt();
			String milestoneLabels = konvertor.from(array[19]).toString();

			newData.setParentFolderId(folderId);
			newData.setParentReqVersionId(parentReqVersionId);
			newData.setProjectId(projectId);
			newData.setVersionId(versionId);
			newData.setRequirementId(requirementId);
			newData.setReference(reference);
			newData.setVersionNumber(versionNumber);
			newData.setCriticality(criticality);
			newData.setStatus(status);
			newData.setCategory(category);
			newData.setCategoryType(categoryType);
			newData.setNbAttachments(nbAttachs);
			newData.setName(name);
			newData.setDescription(description);
			newData.setCreatedBy(createdBy);
			newData.setCreatedOn(createdOn);
			newData.setModifiedBy(modifiedBy);
			newData.setModifiedOn(modifiedOn);
			newData.setTotalVersionNumber(totalVersionNumber);
			newData.setMilestoneLabels(milestoneLabels);

			result.add(newData);
		}

		return result;

	}


	Collection<ReqLinkData> toReqLinkData(Collection<Object[]> tuples) {

		Konvertor konvertor = new Konvertor();
		Collection<ReqLinkData> result = new ArrayList<ReqLinkData>();

		for (Object[] array : tuples) {

			ReqLinkData data = new ReqLinkData();

			Long reqVersionId = konvertor.from(array[0]).toLong();
			Boolean direction = konvertor.from(array[1]).toBoolean();
			String role1 = konvertor.from(array[2]).toString();
			String role2 = konvertor.from(array[3]).toString();
			String project = konvertor.from(array[4]).toString();
			String name = konvertor.from(array[5]).toString();
			Long versionNumber = konvertor.from(array[6]).toLong();

			data.setName(name);
			data.setProject(project);
			data.setReqVersionId(reqVersionId);
			data.setVersionNumber(versionNumber);

			if (direction) {
				data.setRole(role1);
			} else {
				data.setRole(role2);
			}
			result.add(data);
		}

		return result;

	}



	// **************************** DTO binding ***************************

	void bindAllData(Collection<ProjectData> projects, Collection<FolderData> folders,
			Collection<RequirementVersionData> versionData, Collection<TestCaseData> tcData,
			Collection<CufData> cufData, Collection<CufData> rtfCufData,  Collection<CufData> numCufData,
			Collection<CufData> tagCufData,Collection<ReqLinkData> reqLinkData, Map<Long, List<AttachReq>> attachReqDataMap) {

		if (! versionData.isEmpty()) {
			bindTestCases(versionData, tcData);
			bindCufDatas(versionData, cufData);
			bindReqLinkDatas(versionData, reqLinkData);
			bindRtfCufDatas(versionData, rtfCufData);
			bindNumCufDatas(versionData, numCufData);
			bindTagCufDatas(versionData, tagCufData);
			bindAttachReqToReq(versionData, attachReqDataMap);
			bindRequirements(folders, versionData);

		}
		buildFolderHierarchy(folders);
		bindFolders(projects, folders);

	}

	public void bindAttachReqToReq(Collection<RequirementVersionData> requirements, Map<Long, List<AttachReq>> attachReqDataMap) {
		for (RequirementVersionData req : requirements) {
			req.setAttachReqs(attachReqDataMap.get(req.getRequirementId()));
		}
	}

	private void bindTagCufDatas(Collection<RequirementVersionData> versionData, Collection<CufData> tagCufData) {
		for (RequirementVersionData version : versionData) {

			List<CufData> cufs = groupCufsForRequirement(tagCufData, version);

			version.setTagCufs(cufs);
		}
	}

	/** warning : empties testCaseData in the process. */
	private void bindTestCases(Collection<RequirementVersionData> versionData, Collection<TestCaseData> testCaseData) {

		for (RequirementVersionData version : versionData) {

			Iterator<TestCaseData> iterator = testCaseData.iterator();

			while (iterator.hasNext()) {

				TestCaseData next = iterator.next();
				if (version.acceptsAsContent(next)) {
					version.addTestCase(next);
					iterator.remove();
				}

			}

			version.sortContent();
		}

	}

	/** warning : empties cufData in the process. */
	private void bindCufDatas(Collection<RequirementVersionData> versionData, Collection<CufData> cufData) {

		for (RequirementVersionData version : versionData) {

			List<CufData> cufs = groupCufsForRequirement(cufData, version);

			version.setCufs(cufs);
		}

	}

	/** warning : empties rtfCufData in the process. */
	private void bindRtfCufDatas(Collection<RequirementVersionData> versionData, Collection<CufData> rtfCufData) {

		for (RequirementVersionData version : versionData) {

			List<CufData> cufs = groupCufsForRequirement(rtfCufData, version);

			version.setRtfCufs(cufs);
		}

	}

	/** warning : empties reqLinkData in the process. */
	private void bindReqLinkDatas(Collection<RequirementVersionData> versionData, Collection<ReqLinkData> reqLinkData) {

		for (RequirementVersionData version : versionData) {
			List<ReqLinkData> reqLinks = groupReqLinkForRequirement(reqLinkData, version);

			version.setReqLinks(reqLinks);
		}
	}


	/** warning : empties numCufData in the process. */
	private void bindNumCufDatas(Collection<RequirementVersionData> versionData, Collection<CufData> numCufData) {

		for (RequirementVersionData version : versionData) {

			List<CufData> cufs = groupCufsForRequirement(numCufData, version);

			version.setNumCufs(cufs);
		}

	}

	public List<CufData> groupCufsForRequirement(Collection<CufData> cufData, RequirementVersionData version) {
		Iterator<CufData> iterator = cufData.iterator();
		List<CufData> cufs = new ArrayList<CufData>();
		while (iterator.hasNext()) {

			CufData next = iterator.next();
			if (version.acceptsAsCuf(next)) {
				cufs.add(next);
				iterator.remove();
			}

		}
		return cufs;
	}

	/**
	 * warning : empties versionData in the process. Also time consuming. <br/>
	 * note : requirements having no folder will be bound to pseudo root folders of the same project.
	 */
	private void bindRequirements(Collection<FolderData> folderData, Collection<RequirementVersionData> versionData) {

		// map the requirement version data by version id
		Map<Long, RequirementVersionData> mappedVersions = new HashMap<Long, RequirementVersionData>(versionData.size());
		for (RequirementVersionData vData : versionData) {
			mappedVersions.put(vData.getVersionId(), vData);
		}

		// bind requirements having a parent requirement to their parent
		for (RequirementVersionData vData : mappedVersions.values()) {
			if (vData.hasParentRequirementVersion()) {
				RequirementVersionData parent = mappedVersions.get(vData.getParentReqVersionId());
				if (parent != null) {
					parent.addChildrenRequirement(vData);
				} else {
					// Issue 6956 Bind a requirement to a parent folder of his parent requirement if has one
					vData = verifyRootFolder(vData);

					vData.setParentRequirementVersion(null);
					vData.setParentReqVersionId(null);
				}
			}
		}

		/**
		 * bind the requirements having a parent folder to their folder. Have the information cascade to the children
		 * requirements.
		 */
		for (FolderData folder : folderData) {

			Iterator<RequirementVersionData> iterator = versionData.iterator();

			while (iterator.hasNext()) {

				RequirementVersionData version = iterator.next();
				if (folder.acceptsAsContent(version)) {
					folder.addContent(version);
					iterator.remove();
				}
			}
		}

	}

	/**
	 * well hopefully the folders aren't the most represented data, because here we're going for a possible N2 loop.
	 */
	private void buildFolderHierarchy(Collection<FolderData> data) {

		if (data.isEmpty()) {
			return;
		}

		// initialization
		List<FolderData> nonTreatedFolders = new LinkedList<FolderData>();
		nonTreatedFolders.addAll(data);
		Iterator<FolderData> iterator;

		LinkedList<FolderData> beingProcessed = _findPseudoRoot(data);
		FolderData underProcess = beingProcessed.removeFirst();

		// looping
		while (underProcess != null) {

			iterator = nonTreatedFolders.iterator();

			while (iterator.hasNext()) {

				FolderData folder = iterator.next();

				if (folder == underProcess) {
					iterator.remove();
				} else if (underProcess.acceptsAsContent(folder)) {
					folder.setParent(underProcess);
					underProcess.addContent(folder);
					beingProcessed.add(folder);
					iterator.remove();
				}
			}

			underProcess.sortContent();

			underProcess = (beingProcessed.isEmpty()) ? null : beingProcessed.removeFirst();

		}

	}

	/** warnings : empties folders in the process */
	private void bindFolders(Collection<ProjectData> projects, Collection<FolderData> folders) {

		for (ProjectData project : projects) {

			Iterator<FolderData> iterator = folders.iterator();

			while (iterator.hasNext()) {

				FolderData folder = iterator.next();

				if (project.acceptsAsContent(folder)) {
					project.addFolder(folder);
				}

			}

		}

	}

	private LinkedList<FolderData> _findPseudoRoot(Collection<FolderData> data) {

		LinkedList<FolderData> pseudoRoots = new LinkedList<FolderData>();

		for (FolderData folder : data) {
			if (folder.isPseudoRoot()) {
				pseudoRoots.add(folder);
			}
		}

		return pseudoRoots;
	}

	private List<ReqLinkData> groupReqLinkForRequirement(Collection<ReqLinkData> reqLinkData, RequirementVersionData version) {
		Iterator<ReqLinkData> iterator = reqLinkData.iterator();
		List<ReqLinkData> reqLinks = new ArrayList<ReqLinkData>();
		while (iterator.hasNext()) {
			ReqLinkData next = iterator.next();
			if (version.acceptsAsReqLink(next)) {
				reqLinks.add(next);
				iterator.remove();
			}
		}
		return reqLinks;
	}


	// ****************************** private stuff *****************************

	// because a Konvertor is more fun than a mere Converter
	private class Konvertor {

		private Object object;

		Konvertor from(Object toConvert) {
			this.object = toConvert;
			return this;
		}

		@Override
		// it's not really the toString().
		public String toString() {
			if (object == null) {
				return null;
			} else {
				return object.toString();
			}
		}

		public Long toLong() {

			if (object == null) {
				return null;
			}

			Class<?> clazz = object.getClass();
			Long result;

			if (clazz.equals(String.class)) {
				result = Long.valueOf((String) object);
			} else if (clazz.equals(BigInteger.class)) {
				result = ((BigInteger) object).longValue();
			} else if (clazz.equals(Long.class)) {
				result = (Long) object;
			} else if (clazz.equals(Integer.class)) {
				result = ((Integer) object).longValue();
			} else if (clazz.equals(Timestamp.class)) {
				result = ((Timestamp) object).getTime();
			} else if (clazz.equals(Date.class)) {
				result = ((Date) object).getTime();
			} else {
				// well, let's try the leap of faith
				result = Long.valueOf(object.toString());
			}

			return result;
		}

		public Date toDate() {

			if (object == null) {
				return null;
			}

			Class<?> clazz = object.getClass();
			Date result;

			if (clazz.equals(Date.class)) {
				result = (Date) object;
			} else {
				// another leap of faith;
				result = new Date(toLong());
			}

			return result;
		}

		public Integer toInt() {

			if (object == null) {
				return null;
			}

			Class<?> clazz = object.getClass();
			Integer result;

			if (clazz.equals(Integer.class)) {
				result = (Integer) object;
			} else {
				result = Integer.valueOf(toString());
			}

			return result;
		}


		public Boolean toBoolean() {

			if (object == null) {
				return null;
			}

			Class<?> clazz = object.getClass();
			Boolean result = null;

			if (clazz.equals(String.class)) {
				result = Boolean.valueOf((String) object);
			} else if (clazz.equals(Boolean.class)) {
				result =(Boolean) object;
			}
			return result;
		}
	}

	public Collection<CufData> toMultipleValueCufData(Collection<Object[]> tuples) {

		Collection<CufData> cufData = toCufData(tuples);

		for (CufData cuf : cufData) {

			String cufValue = cuf.getValue();
			cufValue = cufValue.replaceAll(SEPARATOR_PLACEHOLDER, TAG_SEPARATOR);
			cufValue = cufValue.substring(0, cufValue.length() - END_SEPARATOR_PLACEHOLDER.length());
			cuf.setValue(cufValue);
		}

		return cufData;
	}

	// Issue 6956 Bind a requirement to a parent folder of his parent requirement if has one
	private RequirementVersionData verifyRootFolder(RequirementVersionData data) {
		RequirementVersionData tempData = data;
		if(tempData.getParentReqVersionId() != null){
			while (tempData.hasParentRequirementVersion()){
				Collection<Long> versionIds =  Arrays.asList(tempData.getParentReqVersionId());
				Collection<Object[]> rawReqVersionData  = queryFinder.getReqVersionsDataForVersionIds(versionIds);
				tempData = toRequirementVersionData(rawReqVersionData).iterator().next();
			}
		}
		data.setParentFolderId(tempData.getParentFolderId());
		return data;
	}

	Map<Long, List<AttachReq>> toAttachReqDataMap(Collection<Object[]> tuples) {
		Konvertor konvertor = new Konvertor();
		Map<Long, List<AttachReq>> result = new HashMap<>();

		for (Object[] array : tuples) {

			AttachReq data = new AttachReq();

			data.setReference(konvertor.from(array[2]).toString());
			data.setName(konvertor.from(array[3]).toString());
			data.setMilestones(konvertor.from(array[4]).toString());
			data.setCriticality(konvertor.from(array[5]).toString());
			data.setParent(konvertor.from(array[6]).toLong());

			addToMap(konvertor.from(array[0]).toLong(), data, result);
		}
		return result;
	}

	// ************************ DTO making *****************************

	static <T> void addToMap(Long key, T data, Map<Long, List<T>> result) {
		if (result.containsKey(key)) {
			result.get(key).add(data);
		} else {
			result.put(key, new ArrayList<>(Collections.singletonList(data)));
		}
	}

}
