/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.dto;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class ProjectData {

	private String name;
	private Long id;

	private Collection<FolderData> folders = new TreeSet<FolderData>(new BreadcrumbComparator());


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<FolderData> getFolders() {
		return folders;
	}

	public void setFolders(List<FolderData> folders) {
		this.folders = folders;
	}

	public void addAllFolders(Collection<FolderData> moreFolders){
		this.folders.addAll(moreFolders);
	}

	public void addFolder(FolderData folder){
		folders.add(folder);
	}



	public boolean acceptsAsContent(FolderData folder){
		return (folder.getProjectId().equals(id));
	}



	private class BreadcrumbComparator implements Comparator<FolderData>{

		public int compare(FolderData folder1, FolderData folder2) {

			int result = compareByBeingRoot(folder1, folder2);

			if (result!=0){
				return result;
			}
			else{
				return compareBreadcrumbs(folder1, folder2);
			}



		}

		private int compareByBeingRoot(FolderData folder1, FolderData folder2){
			if (folder1.isPseudoRoot()){
				return -1;
			}
			else if (folder2.isPseudoRoot()){
				return 1;
			}
			else{
				return 0;
			}

		}

		private int compareBreadcrumbs(FolderData folder1, FolderData folder2){


			String[] crumb1 = folder1.getBreadcrumb().split(" > ");
			String[] crumb2 = folder2.getBreadcrumb().split(" > ");

			int minLength = Math.min(crumb1.length, crumb2.length);

			//first, compare on the names;
			for (int i=0;i<minLength;i++){
				int comp = crumb1[i].compareTo(crumb2[i]);
				if (comp!=0){
					return comp;
				}
			}

			//if not, which one has the shortest breadcrumb ?
			return (crumb1.length < crumb2.length) ? -1 : 1;
		}

	}


}
