/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CufData {
	private String label;

	private String value;

	private String type;

	private Long reqVersionId;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public Date getValueAsDate() throws ParseException {
		return computeValueAsDate(type, value);
	}

	public static Date computeValueAsDate(String type, String value) throws ParseException {
		if (type.equals("DATE_PICKER")) {

			if (value == null || value.isEmpty()) {
					return null;
				} else {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					return sdf.parse(value);
				}

		}
		return null;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getReqVersionId() {
		return reqVersionId;
	}

	public void setReqVersionId(Long reqVersionId) {
		this.reqVersionId = reqVersionId;
	}

	public CufData() {
		super();
	}

}
