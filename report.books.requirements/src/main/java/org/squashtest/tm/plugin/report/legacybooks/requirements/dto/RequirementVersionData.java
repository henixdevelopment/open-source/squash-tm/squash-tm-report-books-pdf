/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class RequirementVersionData {

	private Long parentFolderId;
	private Long parentReqVersionId;
	private Long projectId;
	private Long versionId;
	private Long requirementId;
	private String reference;
	private int versionNumber;
	private String criticality;
	private String status;
	private String category;
	private String categoryType;
	private int nbAttachments;
	private String name;
	private String description;
	private String createdBy;
	private Date createdOn;
	private String modifiedBy;
	private Date modifiedOn;
	private int totalVersionNumber;
	private String milestoneLabels;

	private List<TestCaseData> boundTestCases = new LinkedList<TestCaseData>();

	private List<CufPair> cufPairs = new ArrayList<CufPair>();
	private List<CufData> rtfCufs = new ArrayList<CufData>();
	private List<CufData> numCufs = new ArrayList<CufData>();
	private List<CufData> tagCufs = new ArrayList<CufData>();
	private List<ReqLinkData> reqLinks = new ArrayList<ReqLinkData>();
	private List<AttachReq> attachReqs;

	public List<CufData> getTagCufs() {
		return tagCufs;
	}

	public void setTagCufs(List<CufData> tagCufs) {
		this.tagCufs = tagCufs;
	}

	private List<RequirementVersionData> childrenRequirements = new LinkedList<RequirementVersionData>();

	private RequirementVersionData parentRequirementVersion;

	public Long getParentFolderId() {
		return parentFolderId;
	}

	public void setParentFolderId(Long parentFolderId) {
		this.parentFolderId = parentFolderId;
	}

	public Long getParentReqVersionId() {
		return parentReqVersionId;
	}

	public void setParentReqVersionId(Long parentReqVersionId) {
		this.parentReqVersionId = parentReqVersionId;
	}

	public RequirementVersionData getParentRequirementVersion() {
		return parentRequirementVersion;
	}

	public boolean hasParentRequirementVersion() {
		return (this.parentReqVersionId != null);
	}

	public void setParentRequirementVersion(RequirementVersionData parentRequirementVersion) {
		this.parentRequirementVersion = parentRequirementVersion;
	}

	public void addChildrenRequirement(RequirementVersionData requirement) {
		this.childrenRequirements.add(requirement);
		requirement.setParentRequirementVersion(this);
	}

	public String getParentRequirementDisplayableName() {
		if (hasParentRequirementVersion()) {
			RequirementVersionData parent = this.parentRequirementVersion;
			return parent.getName() + " (ID " + parent.getRequirementId().toString() + ")";
		} else {
			return "";
		}
	}

	public List<RequirementVersionData> getChildren() {
		return this.childrenRequirements;
	}

	public List<RequirementVersionData> getAllChildren() {
		List<RequirementVersionData> allChildren = new LinkedList<RequirementVersionData>(this.childrenRequirements);
		for (RequirementVersionData child : this.childrenRequirements) {
			allChildren.addAll(child.getAllChildren());
		}
		return allChildren;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}

	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Integer getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCategory() {
		return category.toLowerCase();
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<ReqLinkData> getReqLinks() {
		return reqLinks;
	}

	public void setReqLinks(List<ReqLinkData> reqLinks) {
		this.reqLinks = reqLinks;
	}

	public boolean acceptsAsReqLink(ReqLinkData cufData) {
		return cufData.getReqVersionId().equals(versionId);
	}

	public List<AttachReq> getAttachReqs() {
		return attachReqs;
	}

	public void setAttachReqs(List<AttachReq> attachReqs) {
		this.attachReqs = attachReqs;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public int getNbAttachments() {
		return nbAttachments;
	}

	public void setNbAttachments(int nbAttachments) {
		this.nbAttachments = nbAttachments;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifuedOn) {
		this.modifiedOn = modifuedOn;
	}

	public Collection<TestCaseData> getBoundTestCases() {
		return boundTestCases;
	}

	public void setBoundTestCases(List<TestCaseData> boundTestCases) {
		this.boundTestCases = boundTestCases;
	}

	public List<CufPair> getCufPairs() {
		return cufPairs;
	}

	public void setCufs(List<CufData> cufs) {
		Iterator<CufData> it = cufs.iterator();

		while (it.hasNext()) {
			CufData next = it.next();
			CufPair pair = new CufPair();
			pair.setFirstCuf(next);
			if (it.hasNext()) {
				pair.setSecondCuf(it.next());
			}
			this.cufPairs.add(pair);
		}
	}

	public void setRtfCufs(List<CufData> cufs) {
		this.rtfCufs = cufs;
	}

	public List<CufData> getRtfCufs() {

		List<CufData> liste = new ArrayList<CufData>(rtfCufs);
		liste.addAll(getTagCufs());
		return liste;
	}

	public List<CufData> getNumCufs() {
		return numCufs;
	}

	public void setNumCufs(List<CufData> cufs) {
		this.numCufs = cufs;
	}

	public String getMilestoneLabels() {
		return milestoneLabels;
	}

	public void setMilestoneLabels(String milestoneLabels) {
		this.milestoneLabels = milestoneLabels;
	}

	public void addAllTestCases(Collection<TestCaseData> data) {
		boundTestCases.addAll(data);
	}

	public void addTestCase(TestCaseData data) {
		boundTestCases.add(data);
	}

	public int getTotalVersionNumber() {
		return totalVersionNumber;
	}

	public void setTotalVersionNumber(int totalVersionNumber) {
		this.totalVersionNumber = totalVersionNumber;
	}

	public boolean hasAttachments() {
		return (nbAttachments > 0);
	}

	public boolean hasParentFolder() {
		return parentFolderId != null;
	}

	public boolean acceptsAsContent(TestCaseData testCase) {
		return testCase.getRequirementVersionId().equals(versionId);
	}

	public boolean acceptsAsCuf(CufData cufData) {
		return cufData.getReqVersionId().equals(versionId);
	}

	public void sortContent() {
		Collections.sort(boundTestCases, new TestCaseSorter());
	}

	private class TestCaseSorter implements Comparator<TestCaseData> {

		public int compare(TestCaseData tc1, TestCaseData tc2) {
			String name1 = normalizeName(tc1);
			String name2 = normalizeName(tc2);

			return name1.compareTo(name2);
		}

		private String normalizeName(TestCaseData data) {
			if (StringUtils.isNotBlank(data.getReference())) {
				return data.getReference() + " - " + data.getName();
			} else {
				return data.getName();
			}
		}
	}
}
