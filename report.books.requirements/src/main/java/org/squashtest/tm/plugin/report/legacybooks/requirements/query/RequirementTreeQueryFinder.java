/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.squashtest.tm.api.repository.SqlQueryRunner;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RequirementTreeQueryFinder {

	private static final String VERSION_IDS = "versionIds";
	private static final String REQUIREMENT_IDS = "requirementIds";
	private static final String PROJECT_IDS = "projectIds";
	private static final String SQL_FIND_MILESTONE_LABEL = "select label from MILESTONE where milestone_id = :milestoneId";

	private String idsByProjectQuery;
	private String lastVersionIdsByProjectQuery;
	private String idsBySelectionQuery;
	private String lastVersionIdsBySelectionQuery;
	private String idsByMilestoneQuery;
	private String lastVersionIdsByMilestoneQuery;
	private String requirementVersionDataQuery;
	private String folderDataQuery;
	private String projectDataQuery;
	private String boundTCDataQuery;
	private String cufsDataQuery;
	private String rtfCufsDataQuery;
	private String tagCufsDataQuery;
	private String numCufsDataQuery;
	private String idsByTagQuery;
	private String lastVersionIdsByTagQuery;
	private String reqLinkDataQuery;
	private String attachReqDataQuery;
	private String nodeIdsQuery;

	protected SqlQueryRunner runner;

	RequirementTreeQueryFinder() {

	}

	void setLastVersionIdsByTagQuery(String lastVersionIdsByTagQuery) {
		this.lastVersionIdsByTagQuery = lastVersionIdsByTagQuery;
	}

	void setLastVersionIdsBySelectionQuery(String lastVersionIdsBySelectionQuery) {
		this.lastVersionIdsBySelectionQuery = lastVersionIdsBySelectionQuery;
	}

	void setLastVersionIdsByMilestoneQuery(String lastVersionIdsByMilestoneQuery) {
		this.lastVersionIdsByMilestoneQuery = lastVersionIdsByMilestoneQuery;
	}

	void setLastVersionIdsByProjectQuery(String lastVersionIdsByProjectQuery) {
		this.lastVersionIdsByProjectQuery = lastVersionIdsByProjectQuery;
	}

	void setIdsByProjectQuery(String idsByProjectQuery) {
		this.idsByProjectQuery = idsByProjectQuery;
	}

	void setIdsBySelectionQuery(String idsBySelectionQuery) {
		this.idsBySelectionQuery = idsBySelectionQuery;
	}

	void setIdsByMilestoneQuery(String idsByMilestoneQuery) {
		this.idsByMilestoneQuery = idsByMilestoneQuery;
	}

	void setRequirementVersionDataQuery(String requirementVersionDataQuery) {
		this.requirementVersionDataQuery = requirementVersionDataQuery;
	}

	void setFolderDataQuery(String folderDataQuery) {
		this.folderDataQuery = folderDataQuery;
	}

	void setProjectDataQuery(String projectDataQuery) {
		this.projectDataQuery = projectDataQuery;
	}

	void setBoundTCDataQuery(String boundTCDataQuery) {
		this.boundTCDataQuery = boundTCDataQuery;
	}

	public void setCufsDataQuery(String cufsDataQuery) {
		this.cufsDataQuery = cufsDataQuery;
	}

	public void setRtfCufsDataQuery(String rtfCufsDataQuery) {
		this.rtfCufsDataQuery = rtfCufsDataQuery;
	}

	public void setTagCufsDataQuery(String tagCufsDataQuery) {
		this.tagCufsDataQuery = tagCufsDataQuery;
	}

	public void setNumCufsDataQuery(String numCufsDataQuery) {
		this.numCufsDataQuery = numCufsDataQuery;
	}

	public void setIdsByTagQuery(String idsByTagQuery) {
		this.idsByTagQuery = idsByTagQuery;
	}

	public void setReqLinkDataQuery(String reqLinkDataQuery) {
		this.reqLinkDataQuery = reqLinkDataQuery;
	}

	public void setAttachReqDataQuery(String attachReqDataQuery) {
		this.attachReqDataQuery = attachReqDataQuery;
	}

	public void setNodeIdsQuery(String nodeIdsQuery) {
		this.nodeIdsQuery = nodeIdsQuery;
	}

	void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	// ******************************** Id Finding ***********************

	Collection<Long> findIdsByProject(List<Long> projectIds, Boolean printOnlyLastVersion) {
		if (CollectionUtils.isEmpty(projectIds)) {
			return Collections.emptyList();
		}

		Map<String, Collection<Long>> params = new HashMap<String, Collection<Long>>(1);
		params.put("projectIds", projectIds);
		final String query = printOnlyLastVersion ? lastVersionIdsByProjectQuery : idsByProjectQuery;
        List<BigInteger> foundStrIds = runner.executeSelect(query, params);

		return toIdList(foundStrIds);
	}

	Collection<Long> findIdsBySelection(Collection<String> ids, Boolean printOnlyLastVersion, List<Long> projectIds) {

		List<Long> versionIds = new LinkedList<Long>();

		// add the requirements within the given folders
		if (ids != null && (!ids.isEmpty())) {

			Map<String, Collection<Long>> params = new HashMap<String, Collection<Long>>(1);
			params.put("nodeIds", toIdList(ids));
            params.put("projectIds", projectIds);
			String query = printOnlyLastVersion ? lastVersionIdsBySelectionQuery : idsBySelectionQuery;
			List<BigInteger> foundStrIds = runner.executeSelect(query, params);

			versionIds.addAll(toIdList(foundStrIds));
		} else {
			versionIds = new ArrayList<Long>();
		}

		return versionIds;

	}

	Collection<Long> findIdsByMilestone(Collection<String> milestoneIds, Boolean printOnlyLastVersion, List<Long> projectIds) {

		if (!milestoneIds.isEmpty()) {
			Map<String, Collection<Long>> params = new HashMap<String, Collection<Long>>();
			params.put("milestones", toIdList(milestoneIds));
			params.put(PROJECT_IDS, projectIds);
			String query = printOnlyLastVersion ? lastVersionIdsByMilestoneQuery : idsByMilestoneQuery;
			List<BigInteger> foundIds = runner.executeSelect(query, params);
			return toIdList(foundIds);
		} else {
			return Collections.emptyList();
		}

	}


	public Collection<Long> findIdsByTags(List<String> tags, Boolean printOnlyLastVersion, List<Long> projectIds) {

		if (!tags.isEmpty()) {
			Map<String, Object> params = new HashMap<>();
			params.put("tags", tags);
			params.put(PROJECT_IDS, projectIds);
			String query = printOnlyLastVersion ? lastVersionIdsByTagQuery : idsByTagQuery;
			List<BigInteger> foundIds = runner.executeSelect(query, params);
			return toIdList(foundIds);
		} else {
			return Collections.emptyList();
		}
	}

	public Collection<Long> findIdsByVersions(Collection<Long>  versionIds) {
		if (CollectionUtils.isEmpty(versionIds)) {
			return Collections.emptyList();
		}

		Map<String, Collection<Long>> params = new HashMap<>(1);
		params.put(VERSION_IDS, versionIds);
        List<BigInteger> foundStrIds = runner.executeSelect(nodeIdsQuery, params);

		return toIdList(foundStrIds);
	}


	/**
	 * return requirement version data given the REQUIREMENT ids returns the following tuples :
	 * <ol>
	 * <li>folder id (may be null)</li>
	 * <li>parent requirement current version id (may be null)</li>
	 * <li>project id</li>
	 * <li>version id</li>
	 * <li>requirement id</li>
	 * <li>reference</li>
	 * <li>version number</li>
	 * <li>criticality</li>
	 * <li>status</li>
	 * <li>category</li>
	 * <li>nb attachments</li>
	 * <li>name</li>
	 * <li>description</li>
	 * <li>created by</li>
	 * <li>created on</li>
	 * <li>modified by</li>
	 * <li>modified on</li>
	 * <li>totalVersionNumber</li>
	 * </ol>
	 *
	 * @param requirementIds
	 * @return
	 */
	Collection<Object[]> getReqVersionsDataForVersionIds(Collection<Long> requirementIds) {
		return execute(requirementVersionDataQuery, VERSION_IDS, requirementIds);
	}

	/**
	 * return all the test cases data associated to version of the given REQUIREMENT ids. Returns the following tuples :
	 * (project name, test case name, importance, bound requirement version id)
	 *
	 * @param requirementIds
	 * @return
	 */
	Collection<Object[]> getTestCasesDataForVersionIds(Collection<Long> requirementIds) {
		return execute(boundTCDataQuery, VERSION_IDS, requirementIds);
	}

	/**
	 * return all the Cufs data associated to version of the given REQUIREMENT ids. Returns the following tuples : (cuf
	 * label, cuf value, cuf type, bound requirement version id)
	 *
	 * @param requirementIds
	 * @return
	 */
	Collection<Object[]> getCufsDataForVersionIds(Collection<Long> requirementIds) {
		return execute(cufsDataQuery, VERSION_IDS, requirementIds);
	}

	/**
	 * return all the richt text fields Cufs data associated to version of the given REQUIREMENT ids. Returns the
	 * following tuples : (cuf label, cuf value, cuf type, bound requirement version id)
	 *
	 * @param requirementIds
	 * @return
	 */
	Collection<Object[]> getRtfCufsDataForVersionIds(Collection<Long> requirementIds) {
		return execute(rtfCufsDataQuery, VERSION_IDS, requirementIds);
	}

	/**
	 * return all the numeric Cufs data associated to version of the given REQUIREMENT ids. Returns the
	 * following tuples : (cuf label, cuf value, cuf type, bound requirement version id)
	 *
	 * @param requirementIds
	 * @return
	 */
	Collection<Object[]> getNumCufsDataForVersionIds(Collection<Long> requirementIds) {
		return execute(numCufsDataQuery, VERSION_IDS, requirementIds);
	}

	/**
	 * return folder data given the REQUIREMENT ids. Returns the following tuples : (ancestor id, id, project id, name,
	 * description). Ancestor id is null if that folder is at the root of its project. Will not return data for empty
	 * folders.
	 *
	 * @param requirementIds
	 * @return
	 */
	Collection<Object[]> getFoldersDataForVersionIds(Collection<Long> requirementIds) {
		return execute(folderDataQuery, VERSION_IDS, requirementIds);
	}

	/**
	 * return project data given the REQUIREMENT ids returns tuples of (id, name)
	 *
	 * @param requirementIds
	 * @return
	 */
	Collection<Object[]> getProjectDataForVersionIds(Collection<Long> requirementIds) {
		return execute(projectDataQuery, VERSION_IDS, requirementIds);
	}

	public Collection<Object[]> getreqLinkDataQueryForReqIds(Collection<Long> requirementIds) {
		return execute(reqLinkDataQuery, VERSION_IDS, requirementIds);
	}

	Collection<Object[]> getAttachReqForReqIds(Collection<Long> requirementIds) {
		return execute(attachReqDataQuery, REQUIREMENT_IDS, requirementIds);
	}

	String getMilestoneLabel(Integer milestoneId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("milestoneId", milestoneId);
		return runner.executeUniqueSelect(SQL_FIND_MILESTONE_LABEL, params);
	}

	// ************************** utils **************************************

	private Collection<Object[]> execute(String query, String paramName, Collection<Long> ids) {

		if (ids.isEmpty()) {
			return Collections.emptyList();
		}

		Map<String, Collection<Long>> params = new HashMap<String, Collection<Long>>(1);

		params.put(paramName, ids);

		return runner.executeSelect(query, params);
	}

	@SuppressWarnings("unchecked")
	protected Collection<Long> toIdList(Collection<?> ids) {
		return CollectionUtils.collect(ids, new IdTransformer());
	}

	// dirty impl
	private static class IdTransformer implements Transformer {
		public Object transform(Object arg0) {
			Class<?> argClass = arg0.getClass();

			if (argClass.equals(String.class)) {
				return Long.valueOf((String) arg0);
			} else if (argClass.equals(BigInteger.class)) {
				return ((BigInteger) arg0).longValue();
			} else if (argClass.equals(Integer.class)) {
				return Long.valueOf((Integer) arg0);
			} else {
				throw new RuntimeException("bug : IdTransformer cannto convert items of class " + argClass.getName());
			}
		}
	}

	public Collection<Object[]> getTagCufsDataForVersionIds(Collection<Long> requirementIds) {
		return execute(tagCufsDataQuery, VERSION_IDS, requirementIds);
	}


}
