/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class FolderData {

	private static final String BREADCRUMB_SEPARATOR = " > ";

	private String name = "";
	private String description;
	private Long folderId;
	private Long ancestorId;
	private Long projectId;

	private FolderData parent;
	private String breadcrumb;
	private String index=null;

	private boolean pseudoRoot=false;

	private List<FolderData> folders = new LinkedList<FolderData>();
	private List<RequirementVersionData> requirementVersions = new LinkedList<RequirementVersionData>();


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}

	public Long getAncestorId() {
		return ancestorId;
	}

	public void setAncestorId(Long ancestorId) {
		this.ancestorId = ancestorId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public boolean isPseudoRoot() {
		return pseudoRoot;
	}

	public void setPseudoRoot(boolean pseudo){
		pseudoRoot = pseudo;
	}

	public void sortContent() {
		Collections.sort(folders, new FolderSorter());
		Collections.sort(requirementVersions, new VersionSorter());
	}

	public FolderData getParent() {
		return parent;
	}

	public void setParent(FolderData parent) {
		this.parent = parent;
	}

	public List<FolderData> getFolders() {
		return folders;
	}

	public List<RequirementVersionData> getRequirementVersions() {
		return requirementVersions;
	}

	public void addContent(FolderData folder) {
		folders.add(folder);
	}

	public void addContent(RequirementVersionData version) {
		requirementVersions.add(version);
		requirementVersions.addAll(version.getAllChildren());
	}



	public String getBreadcrumb() {

		if (breadcrumb == null) {
			if (isPseudoRoot() || parent.isPseudoRoot()) {
				breadcrumb = name;
			} else {
				breadcrumb = parent.getBreadcrumb() + BREADCRUMB_SEPARATOR
						+ name;
			}
		}

		return breadcrumb;
	}


	public String getIndex() {
		if (index==null){
			if (isPseudoRoot()){
				index = "";
			}
			else{
				index = getParent()._tellMyIndex(this);
			}
		}
		return index;
	}


	private String _tellMyIndex(FolderData son){

		String sonRank = Integer.valueOf(folders.indexOf(son)+1).toString();

		if (this.isPseudoRoot()){
			return sonRank;
		}
		else{
			return getIndex()+"."+sonRank;
		}

	}

	public boolean hasAncestor() {
		return ancestorId != null;
	}


	public boolean acceptsAsContent(FolderData content) {

		if (content == this){
			return false;
		}

		if (isPseudoRoot()) {

			return ((!content.hasAncestor()) && (content.projectId
					.equals(projectId)));

		} else {
			return (content.hasAncestor() && content.ancestorId
					.equals(folderId));
		}
	}


	public boolean acceptsAsContent(RequirementVersionData content) {

		if (isPseudoRoot()) {
			//a requirement belongs to the root folder it if has no other kind of parent (neither folder nor requirement)
			return ((!content.hasParentFolder()) &&
					(!content.hasParentRequirementVersion()) &&
					(content.getProjectId().equals(projectId)));

		} else {
			return (content.hasParentFolder() && content.getParentFolderId()
					.equals(folderId));
		}
	}



	private static class FolderSorter implements Comparator<FolderData> {

		public int compare(FolderData arg0, FolderData arg1) {
			return arg0.getName().compareTo(arg1.getName());
		}

	}


	private static class VersionSorter implements Comparator<RequirementVersionData> {

		public int compare(RequirementVersionData o1, RequirementVersionData o2) {

			int order = compareByName(o1, o2);

			if (order==0){
				order = compareById(o1, o2);
			}

			if (order==0){
				order = compareByVersion(o1, o2);
			}

			return order;
		}


		private int compareByName(RequirementVersionData o1, RequirementVersionData o2){

			String name1 = normalizeName(o1);
			String name2 = normalizeName(o2);

			return name1.compareTo(name2);

		}


		private int compareById(RequirementVersionData o1, RequirementVersionData o2){
			return o1.getRequirementId().compareTo(o2.getRequirementId());
		}


		private int compareByVersion(RequirementVersionData o1, RequirementVersionData o2){
			return o1.getVersionNumber().compareTo(o2.getVersionNumber());
		}

		private String normalizeName(RequirementVersionData data){
			if (StringUtils.isNotBlank(data.getReference())){
				return data.getReference()+" - "+data.getName();
			}
			else{
				return data.getName();
			}
		}
	}

}
