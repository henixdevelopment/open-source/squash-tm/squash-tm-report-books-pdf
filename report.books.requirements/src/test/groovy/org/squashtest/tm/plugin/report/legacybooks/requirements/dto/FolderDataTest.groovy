/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.dto

import spock.lang.Specification

class FolderDataTest extends Specification {

	def "should return the index of a son folder"(){

		given :
			FolderData root = new FolderData()
			FolderData parent = new FolderData()
			FolderData son1 = new FolderData()
			FolderData son2 = new FolderData()
			FolderData son3 = new FolderData()

		and :
			root.setPseudoRoot( true)
			root.addContent  parent
			parent.parent = root

			parent.folders = [son1, son2, son3]
			[son1, son2, son3].each{it.parent = parent}


		when :
			def parentIndex = parent.index;
			def sonsIndex = parent.folders.collect {it.index}


		then :
			parentIndex == "1"
			sonsIndex == ["1.1", "1.2", "1.3"]
	}



}
