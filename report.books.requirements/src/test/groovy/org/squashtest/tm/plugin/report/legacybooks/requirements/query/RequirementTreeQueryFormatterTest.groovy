/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.query

import org.squashtest.tm.plugin.report.legacybooks.requirements.dto.FolderData
import spock.lang.Specification

class RequirementTreeQueryFormatterTest extends Specification {

	def "should bind together a bunch of folders"(){

		given :
			FolderData rootFolder1 = makeRootFolder(1l)
			FolderData rootFolder2 = makeRootFolder(2l)

			FolderData folder11 =  makeFolder(1l, null, 11l, "folder11")
			FolderData folder12 =  makeFolder(1l, null, 12l, "folder12")
			FolderData folder121 = makeFolder(1l, 12l, 121l, "folder121")


			FolderData folder21 = makeFolder(2l, null, 21l, "folder21")
			FolderData folder22 =  makeFolder(2l, null, 22l, "folder22")
			FolderData folder211 = makeFolder(2l, 21l, 211l, "folder211")


		and :
			def allFolders = [
				folder211,
				folder21,
				folder12,
				rootFolder2,
				folder22,
				folder121,
				folder11,
				rootFolder1
			]


		when :
			RequirementTreeQueryFormatter formatter = new RequirementTreeQueryFormatter()
			formatter.buildFolderHierarchy(allFolders)


		then :

			rootFolder1.folders as Set == [folder11, folder12] as Set
			folder11.folders as Set == [] as Set
			folder12.folders as Set == [ folder121 ] as Set
			folder121.folders as Set == [  ] as Set

			rootFolder2.folders as Set == [folder21, folder22] as Set
			folder21.folders as Set == [ folder211] as Set
			folder22.folders as Set == [  ] as Set
			folder211.folders as Set == [  ] as Set


	}


	def makeFolder( projectId, ancestor, id, name){

		FolderData data = new FolderData()

		data.folderId = id
		data.ancestorId = ancestor
		data.projectId = projectId
		data.name = name

		return data

	}

	def makeRootFolder(projectId){

		FolderData data = new FolderData();
		data.projectId = projectId
		data.setPseudoRoot(true)

		return data
	}



}
