/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.legacybooks.requirements.dto

import spock.lang.Specification

class ProjectDataTest extends Specification {

	def "should correctly enumerate folders"(){

		given :
			def unsorted_breadcrumbs = [
				"test 1 > sous test 1 > details",
				"test 1 > autre chose > uh",
				"test 1 > sous test 1",
				"test 1 > something else",
				"test 1 > sous test 1 > extensions",
				"uh",
				"",
				"test 1"
			]

			def sorted_breadcrumbs = [
				"",
				"test 1",
				"test 1 > autre chose > uh",
				"test 1 > something else",
				"test 1 > sous test 1",
				"test 1 > sous test 1 > details",
				"test 1 > sous test 1 > extensions",
				"uh"
			]


		and :
			def folders = []
			unsorted_breadcrumbs.each{ folders << mockFolderData(it) }

		and :
			ProjectData project = new ProjectData()

		when :
			project.addAllFolders(folders)

			def enumerated = []
			//purposedly written java style
			for (FolderData folder : project.getFolders()){
				enumerated << folder.getBreadcrumb()
			}

		then :
			enumerated == sorted_breadcrumbs

	}





	def mockFolderData(String breadcrumb){

		FolderData folder = Mock()

		if (breadcrumb == ""){
			folder.isPseudoRoot() >> true
		}
		else{
			folder.isPseudoRoot() >> false
		}

		folder.getBreadcrumb() >> breadcrumb

		return folder

	}

}
